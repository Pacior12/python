import random

capitals = {
    'Alabama': 'Montgomery',
    'Alaska': 'Juneau',
    'Arizona': 'Phoenix',
    'Arkansas': 'Little Rock',
    'California': 'Sacramento',
    'Colorado': 'Denver',
    'Connecticut': 'Hartford',
    'Delaware': 'Dover',
    'Florida': 'Tallahassee',
    'Georgia': 'Atlanta',
}

capitals_len = int(len(capitals))

chosen_state = random.choice(list(capitals))

capital_of_chosen_state = capitals[chosen_state].lower()


while (1 > 0):
    z = input(f"What's the capital of {chosen_state}?: ")
    if (z.lower() == capital_of_chosen_state):
        print("Correct!")
        break
    if (z.lower() == "exit"):
        print(f"The correct answer was: {capital_of_chosen_state}")
        print("Goodbye")
        break
    else:
        print("Wrong!")



