import threading
import psycopg2
import time

database_config = {
    "host": "localhost",
    "dbname": "postgres",
    "user": "postgres",
    "password": "1234",
    "port": 5432
}


class ConnectionPool:
    def __init__(self):
        self.connections = []
        self.lock = threading.Lock()

    def create_connection(self):
        conn = Connection()
        self.connections.append(conn)

    def process_command(self, command):
        found_connection = False
        with self.lock:
            for connection in range (self.connections):
            if len(self.connections) == 0 and len(self.used_connections) < 100:
                self.create_connection()
            if len(self.connections) == 0 and len(self.used_connections) == 100:
                print("Max number of connections reached!")
                return
            if len(self.connections) > 0:
                conn = self.connections.pop()
                self.connections.append(conn)
                conn.available = False
            else:
                return
            try:
                conn.cur.execute(command)
                conn.connection.commit()
                conn.available = True
            except (Exception, psycopg2.Error) as e:
                print(f'Unable to execute command: {command}\n{e}')
                self.connections.remove(conn)
                del conn
                self.create_connection()

    def delete_connections(self):
        with self.lock:
            timer = time.perf_counter()
            for conn in self.connections[:]:
                if conn.available and len(self.connections) > 10 and timer - conn.time_inactive > 60:
                    conn.cur.close()
                    conn.connection.close()
                    self.connections.remove(conn)
                    del conn




class Connection:
    def __init__(self):
        self.connection = psycopg2.connect(**database_config)
        self.cur = self.connection.cursor()
        self.available = True
        self.time_inactive = time.perf_counter()


def create_initial_connections(pool):
    for i in range(10):
        pool.create_connection()


pool = ConnectionPool()
create_initial_connections(pool)

pool.process_command("""
    SELECT name 
    FROM test 
    JOIN generate_series(1, 10000) AS gs(i) ON TRUE 
    WHERE name = 'test';
""")


while True:
    print(pool.connections)
    time.sleep(5)
    pool.delete_connections()
