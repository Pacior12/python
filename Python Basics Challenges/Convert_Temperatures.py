temp1 = input("Enter a temperature in degrees F: ")

def convert_far_to_cel(temp1):
    C = (float(temp1) - 32) * 5/9
    return C
    
tempc = convert_far_to_cel(temp1)
rounded_tempc = round(tempc, 2)
print(f"{temp1} degrees F = {rounded_tempc:.2f} degrees C")

temp2 = input("Enter a temperature in degrees C: ")

def convert_cel_to_far(temp2):
    F = float(temp2) * 9/5 + 32
    return F

tempf = convert_cel_to_far(temp2)
rounded_tempf = round(tempf, 2)
print(f"{temp2} degrees C = {rounded_tempf:.2f} degrees F")
