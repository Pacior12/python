from itertools import combinations
string, k = input().split()
k1 = int(k)
upString = sorted(string.upper())
count = 1
comb = []
while count < k1+1:
  comb = list(combinations(upString, count))
  for i in comb:
    print(''.join(i))
  count +=1  
