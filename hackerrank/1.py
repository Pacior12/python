def print_formatted(number):
    width = len(bin(number)[2:])
    print(width)# Calculate the width based on the binary representation

    for i in range(1, number + 1):
        decimal_value = str(i)
        octal_value = oct(i)[2:]
        hexadecimal_value = hex(i)[2:].upper
        binary_value = bin(i)[2:]

        # Format and print the values
        print(f"{decimal_value.rjust(width)} {octal_value.rjust(width)} {hexadecimal_value.rjust(width)} {binary_value.rjust(width)}")

print_formatted(32)
