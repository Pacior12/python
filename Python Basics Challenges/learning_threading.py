import time
import concurrent.futures

list_of_letters = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k"]

list1 = []

t1 = time.perf_counter()

def append_letters(letter):
    list1.append(letter)
    time.sleep(1)
    
with concurrent.futures.ThreadPoolExecutor() as executor:
        executor.map(append_letters, list_of_letters)

t2 = time.perf_counter()

print(list1)

print(f"Process 1 finished in {t2-t1} seconds!")

list2 = []

t3 = time.perf_counter()
for i in range(len(list_of_letters)):
    list2.append(list_of_letters[i])
    time.sleep(1)
t4 = time.perf_counter()
print(list2)
print(f"Process 2 finished in {t4-t3} seconds!")
