number = input("Enter a positive integer: ")

int_number = int(number)

for i in range(1, (int_number + 1)):
    if ((int_number % i) == 0):
        print(f"{i} is a factor of {number}")
