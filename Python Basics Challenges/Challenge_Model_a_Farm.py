class Animal:

    number_of_legs = 4
    food = "Grass"
    sleep_time = "4 hours per day"
    
    def __init__(self, name, color):
        self.name = name
        self.color = color

    def voice(self, sound = None):
        if sound is None:
            return "Am i even speaking?"
        return f"{self.name} says {sound}"

class Sheep(Animal):
    sleep_time = "11 hours per day"
    def voice(self, sound="Mee!"):
        return super().voice(sound)

class Pig(Animal):
    def voice(self, sound="Oink Oink!"):
        return super().voice(sound)
    
class Cow(Animal):
    def voice(self, sound="Moo!"):
        return super().voice(sound)


