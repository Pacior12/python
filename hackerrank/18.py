set_1_size = int(input())

set_1 = set(map(int, input().split()))

set_2_size = int(input())

set_2 = set(map(int, input().split()))

difference1 = set_1.difference(set_2)

difference2 = set_2.difference(set_1)

difference1.update(difference2)

sorted_difference = sorted(difference1)

for i in range(len(sorted_difference)):
    print(sorted_difference[i])
