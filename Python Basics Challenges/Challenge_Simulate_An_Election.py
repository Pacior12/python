import random

def voting():
    candidate_a_wins = 0
    if random.random() < 0.87:
        candidate_a_wins = candidate_a_wins + 1
    if random.random() < 0.65:
        candidate_a_wins = candidate_a_wins + 1
    if random.random() < 0.17:
        candidate_a_wins = candidate_a_wins + 1
    return candidate_a_wins

total = 0

for n in range(1000):
    score = voting()
    if score > 1:
        total = total + 1
        
percentage = (total/10)
print (f"Candidate A wins the election {percentage}% of times!")
