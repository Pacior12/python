n = int(input())
A = set(map(int, input().split(" ")))
sets = int(input())
for _ in range(sets):
    op = input().split(" ")
    B = set(map(int, input().split(" ")))
    if op[0] == "intersection_update":
         A = A & B
    elif op[0] == "update":
         A = A | B
    elif op[0] == "symmetric_difference_update":
        A = A ^ B
    elif op[0] == "difference_update":
        A = A - B
print(sum(A))
