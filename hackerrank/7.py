import calendar

m, d, y = map(int, input().split())
print((calendar.day_name[calendar.datetime.date(y, m, d).weekday()]).upper())
