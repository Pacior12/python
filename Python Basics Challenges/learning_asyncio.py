import asyncio

async def main():
    task = asyncio.create_task(printTo10())
    await task
    task2 = asyncio.create_task(printTo20())
    await task2
    for i in range(21, 31):
        print(i)
        await asyncio.sleep(0.2)

async def printTo10():
    for i in range(0,11):
        print(i)
        await asyncio.sleep(0.2)

async def printTo20():
    for i in range(11, 21):
        print(i)
        await asyncio.sleep(0.2)
        
asyncio.run(main())
