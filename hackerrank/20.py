from collections import deque

d = deque()
N = int(input())

for i in range(N):
    cmd = input()
    try:
        cmd, arg = cmd.split()
        if cmd == 'append':
            d.append(arg)
        if cmd == 'appendleft':
            d.appendleft(arg)
    except ValueError:
        if cmd == 'pop':
            d.pop()
        if cmd == 'popleft':
            d.popleft()
print(*d)
