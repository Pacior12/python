import socket as s
import json
import time
import psycopg2
from server_class import Server
from notation_class import Notation
from database_class import Database


keep_going = True

db_host = "localhost"
db_name = "postgres"
db_user = "postgres"
db_password = "1234"
db_port = 5432

HOST = "127.0.0.1"
PORT = 65432
BUFFER = 1024


max_number_of_messages = 5
max_database_string_length = 255

notation = Notation()

database = Database(db_host, db_name, db_user, db_password, db_port)
database.connect_to_database()
database.create_table()
database.add_initial_admin_account()

server = Server(HOST, PORT, BUFFER, database, notation, max_number_of_messages, max_database_string_length)
server.connect()
server.send_initial_message()

while keep_going:
    while not server.logged:
        combined_login_data = server.client_socket.recv(BUFFER).decode("utf-8")
        login_data = notation.decode_json(combined_login_data)
        choice, login, password = login_data.values()
        login, server.logged, response = server.process_login_and_return_response(server.logged, choice, login, password)
        if response == f"Successfully logged in as {login}":
            print(f"{login} connected")
            number_of_messages = database.select_data_by_name("number_of_users_messages", login)
            initial_message = {
                'response': response,
                'info_message': "Type 'info' for the list of avaiable commands",
                'messages_message': f"You have {number_of_messages} messages in your mailbox!",
                'read_messages': "Type 'read_messages' to read them!"

            }
            combined_initial_message = notation.encode_json(initial_message)
            server.client_socket.send(combined_initial_message.encode("utf8"))
            server.logged = True
        else:
            initial_message = {
                'response': response
            }
            combined_initial_message = notation.encode_json(initial_message)
            server.client_socket.send(combined_initial_message.encode("utf8"))
    while server.logged:
        command_json = server.client_socket.recv(BUFFER).decode("utf-8")
        command = notation.decode_json(command_json)
        server.logged, response = server.process_command_and_return_response(server.logged, login, command)
        response_json = notation.encode_json(response)
        server.client_socket.send(response_json.encode("utf8"))
        if response == "Connection terminated":
            print(f"{login} has terminated the connection")
            keep_going = False
            break

