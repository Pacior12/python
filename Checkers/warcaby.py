import time
import random

number_of_alive_o = 12
number_of_alive_x = 12

board = [
    ["   ", " x ", "   ", " x ", "   ", " x ", "   ", " x ", ],
    [" x ", "   ", " x ", "   ", " x ", "   ", " x ", "   ", ],
    ["   ", " x ", "   ", " x ", "   ", " x ", "   ", " x ", ],
    ["   ", "   ", "   ", "   ", "   ", "   ", "   ", "   ", ],
    ["   ", "   ", "   ", "   ", "   ", "   ", "   ", "   ", ],
    [" o ", "   ", " o ", "   ", " o ", "   ", " o ", "   ", ],
    ["   ", " o ", "   ", " o ", "   ", " o ", "   ", " o ", ],
    [" o ", "   ", " o ", "   ", " o ", "   ", " o ", "   ", ],
]


def converter_for_player(white_figures_with_possible_beatings):
    numbers = ["0", "1", "2", "3", "4", "5", "6", "7", "8"]
    letters = ["A", "B", "C", "D", "E", "F", "G", "H", "J"]
    list_for_player = []
    for i in range(len(white_figures_with_possible_beatings)):
        for j in range(9):
            if white_figures_with_possible_beatings[i][1] == numbers[j]:
                letter = letters[j]
            if white_figures_with_possible_beatings[i][0] == numbers[j]:
                number = numbers[j + 1]
        list_for_player.append(f"{letter}{number}")
    return list_for_player


def converter_for_computer(white_figures_with_possible_beatings):
    numbers = ["0", "1", "2", "3", "4", "5", "6", "7", "8"]
    letters = ["A", "B", "C", "D", "E", "F", "G", "H", "J"]
    for i in range(len(white_figures_with_possible_beatings)):
        for j in range(9):
            if white_figures_with_possible_beatings[0] == letters[j]:
                number = int(numbers[j])
            if white_figures_with_possible_beatings[1] == numbers[j]:
                letter = int(numbers[j - 1])
    return letter, number


def line_converter(board, num):
    s = str(board[num])
    s = s.replace(',', ' ')
    s = s.replace("'", ' ')
    return s


def print_board(board):
    print("          A      B      C      D      E      F      G      H")
    print(f"1:     {line_converter(board, num=0)}")
    print(f"2:     {line_converter(board, num=1)}")
    print(f"3:     {line_converter(board, num=2)}")
    print(f"4:     {line_converter(board, num=3)}")
    print(f"5:     {line_converter(board, num=4)}")
    print(f"6:     {line_converter(board, num=5)}")
    print(f"7:     {line_converter(board, num=6)}")
    print(f"8:     {line_converter(board, num=7)}")


directions_names = ["upright", "downright", "upleft", "downleft"]

directions = {"upright": [-2, 2, 2, 8, 0, 6],
              "downright": [2, 2, 0, 6, 0, 6],
              "upleft": [-2, -2, 2, 8, 2, 8],
              "downleft": [2, -2, 0, 6, 2, 8]}

number_of_directions = 4


def possible_squares_across(row, col):
    squares_until_top = 0 + row
    squares_until_bottom = 7 - row
    squares_until_left = 0 + col
    squares_until_right = 7 - col
    upright_squares = squares_until_top
    if upright_squares > squares_until_right:
        upright_squares = squares_until_right
    upleft_squares = squares_until_top
    if upleft_squares > squares_until_left:
        upleft_squares = squares_until_left
    downright_squares = squares_until_bottom
    if downright_squares > squares_until_right:
        downright_squares = squares_until_right
    downleft_squares = squares_until_bottom
    if downleft_squares > squares_until_left:
        downleft_squares = squares_until_left
    return upright_squares, downright_squares, upleft_squares, downleft_squares


def set_direction(row1, col1, row2, col2):
    if row1 > row2 and col1 < col2:
        direction = "upright"
    elif row1 > row2 and col1 > col2:
        direction = "upleft"
    elif row1 < row2 and col1 > col2:
        direction = "downleft"
    elif row1 < row2 and col1 < col2:
        direction = "downright"
    else:
        direction = None
    return direction


def set_number_of_squares(row1, row2):
    if row1 > row2:
        number_of_squares = row1 - row2
    if row2 > row1:
        number_of_squares = row2 - row1
    return number_of_squares


def check_game_winner(number_of_alive_o, number_of_alive_x):
    white_figures_with_possible_moves, black_figures_with_possible_moves = figures_with_possible_moves(board)
    possible_moves = False
    game_ending_black = False
    game_ending_white = False
    game_ending = False
    if white_figures_with_possible_moves == []:
        game_ending_black = True
        possible_moves = True
    if black_figures_with_possible_moves == []:
        game_ending_white = True
        possible_moves = True
    if number_of_alive_x == 0:
        game_ending_white = True
    if number_of_alive_o == 0:
        game_ending_black = True
    if game_ending_black == True:
        print_board(board)
        print("Black has won!")
        if possible_moves == True:
            print("White has no possible moves!")
        print("Thank You for playing!")
        game_ending = True
    if game_ending_white == True:
        print_board(board)
        print("White has won!")
        if possible_moves == True:
            print("Black has no possible moves!")
        print("Thank You for playing!")
        game_ending = True
    return game_ending


def move(board, row1, col1, row2, col2, figure):
    board[row1][col1] = "   "
    board[row2][col2] = figure
    return board


def check_possible_beatings_for_chosen_field(board, row, col):
    if board[row][col] == 5:
        pass


def queen_converter(board):
    for k in range(0, 8):
        if board[0][k] == " o ":
            board[0][k] = " O "
        if board[7][k] == " x ":
            board[7][k] = " X "
    return board


def check_possible_beatings_for_pawn(board, row, col, direction, enemy, beatings_list):
    if row in range(directions[direction][2], directions[direction][3]) and col in range(directions[direction][4],
                                                                                         directions[direction][5]):
        check_if_empty = board[row + directions[direction][0]][col + directions[direction][1]]
        if check_if_empty == "   " and (
                (board[row + int(directions[direction][0] / 2)][col + int(directions[direction][1] / 2)] == enemy[
                    0]) or (board[row + int(directions[direction][0] / 2)][col + int(directions[direction][1] / 2)] ==
                            enemy[1])):
            beatings_list.append(f"{row + directions[direction][0]}{col + directions[direction][1]}")
    return beatings_list


def check_possible_beatings_for_queen(board, row, col, direction, enemy, ally, beatings_list, squares):
    if squares > 1:
        for k in range(1, squares):
            if (board[row + (k * int(directions[direction][0] / 2))][col + (k * int(directions[direction][1] / 2))] ==
                enemy[0]) or (board[row + (k * int(directions[direction][0] / 2))][
                                  col + (k * int(directions[direction][1] / 2))] == enemy[1]):
                if board[row + ((k + 1) * int(directions[direction][0] / 2))][
                    col + ((k + 1) * int(directions[direction][1] / 2))] == "   ":
                    beatings_list.append(
                        f'{row + ((k + 1) * int(directions[direction][0] / 2))}{col + ((k + 1) * int(directions[direction][1] / 2))}')
                else:
                    break
    return beatings_list


def check_possible_beating_queen(board, row, col, direction, enemy, ally, beatings_list, squares, should_continue):
    if squares > 1 and should_continue:
        for k in range(1, squares + 1):
            if (board[row + (k * int(directions[direction][0] / 2))][col + (k * int(directions[direction][1] / 2))] ==
                ally[0]) or (board[row + (k * int(directions[direction][0] / 2))][
                                  col + (k * int(directions[direction][1] / 2))] == ally[1]):
                break
            if (board[row + (k * int(directions[direction][0] / 2))][col + (k * int(directions[direction][1] / 2))] ==
                enemy[0]) or (board[row + (k * int(directions[direction][0] / 2))][
                                  col + (k * int(directions[direction][1] / 2))] == enemy[1]):
                if board[row + ((k + 1) * int(directions[direction][0] / 2))][
                    col + ((k + 1) * int(directions[direction][1] / 2))] == "   ":
                    beatings_list.append(f'{row}{col}')
                    should_continue = False
                else:
                    break

    return beatings_list, should_continue


white_pawns = [" o ", " O "]
black_pawns = [" x ", " X "]

def beating(number_of_alive_o, number_of_alive_x, direction, board, figure, number_of_squares, row, col):
    if figure == " o " or figure == " O ":
        enemy_pawns = black_pawns
    if figure == " x " or figure == " X ":
        enemy_pawns = white_pawns
    board[row][col] = "   "
    board_index_row = row + (int(number_of_squares) * (int(directions[direction][0] / 2)))
    board_index_col = col + (int(number_of_squares) * int(directions[direction][1] / 2))
    board[board_index_row][board_index_col] = figure
    for k in range(1, number_of_squares + 1):
        next_row = row + k * int(directions[direction][0] / 2)
        next_col = col + k * int(directions[direction][1] / 2)
        if board[next_row][next_col] in enemy_pawns:
            board[next_row][next_col] = "   "
            if enemy_pawns == black_pawns:
                number_of_alive_x = number_of_alive_x - 1
            if enemy_pawns == white_pawns:
                number_of_alive_o = number_of_alive_o - 1
    return board, number_of_alive_o, number_of_alive_x

def check_possible_moves_queen(board, row, col, direction, moves_list, squares, should_continue):
    if squares > 0 and should_continue == True:
        for k in range(1, squares + 1):
            if (board[row + (k * int(directions[direction][0] // 2))][
                    col + (k * int(directions[direction][1] // 2))] == "   ") and should_continue == True:
                moves_list.append(f'{row}{col}')
                should_continue = False
            else:
                break
    return moves_list, should_continue


def check_possible_moves_for_queen(board, row, col, direction, moves_list, squares):
    if squares > 0:
        for k in range(1, squares + 1):
            if (board[row + (k * int(directions[direction][0] // 2))][
                col + (k * int(directions[direction][1] // 2))] == "   "):
                moves_list.append(
                    f'{row + (k * int(directions[direction][0] // 2))}{col + (k * int(directions[direction][1] // 2))}')
            else:
                break
    return moves_list


def check_possible_moves_pawn(board, row, col, figure, moves_list):
    should_continue = True
    if figure == " x ":
        for direction in ["downright", "downleft"]:
            if direction == "downright" and col < 7 or direction == "downleft" and col > 0:
                if board[row + int(directions[direction][0] / 2)][
                    col + int(directions[direction][1] / 2)] == "   " and should_continue == True:
                    moves_list.append(f"{row}{col}")
                    should_continue = False
    if figure == " o ":
        for direction in ["upright", "upleft"]:
            if direction == "upright" and col < 7 or direction == "upleft" and col > 0:
                if board[row + int(directions[direction][0] / 2)][
                    col + int(directions[direction][1] / 2)] == "   " and should_continue == True:
                    moves_list.append(f"{row}{col}")
                    should_continue = False
    return moves_list


def check_possible_moves_for_pawn(board, row, col, figure, moves_list):
    if figure == " x ":
        for direction in ["downright", "downleft"]:
            if direction == "downright" and col < 7 or direction == "downleft" and col > 0:
                if board[row + int(directions[direction][0] / 2)][col + int(directions[direction][1] / 2)] == "   ":
                    moves_list.append(
                        f"{row + int(directions[direction][0] / 2)}{col + int(directions[direction][1] / 2)}")
    if figure == " o ":
        for direction in ["upright", "upleft"]:
            if direction == "upright" and col < 7 or direction == "upleft" and col > 0:
                if board[row + int(directions[direction][0] / 2)][col + int(directions[direction][1] / 2)] == "   ":
                    moves_list.append(
                        f"{row + int(directions[direction][0] / 2)}{col + int(directions[direction][1] / 2)}")
    return moves_list


def check_possible_beating_pawn(board, row, col, direction, enemy, beatings_list, should_continue):
    if row in range(directions[direction][2], directions[direction][3]) and col in range(directions[direction][4],
                                                                                         directions[direction][
                                                                                             5]) and should_continue == True:
        check_if_empty = board[row + directions[direction][0]][col + directions[direction][1]]
        if check_if_empty == "   " and (
                (board[row + int(directions[direction][0] / 2)][col + int(directions[direction][1] / 2)] == enemy[
                    0]) or (board[row + int(directions[direction][0] / 2)][col + int(directions[direction][1] / 2)] ==
                            enemy[1])):
            beatings_list.append(f"{row}{col}")
            should_continue = False
    return beatings_list, should_continue


def figures_with_possible_beatings(board):
    white_figures_with_possible_beatings = []
    black_figures_with_possible_beatings = []
    for row in range(0, 8):
        for col in range(0, 8):
            if board[row][col] == " o ":
                should_continue = True
                for k in range(number_of_directions):
                    white_figures_with_possible_beatings, should_continue = check_possible_beating_pawn(board, row, col,
                                                                                                        directions_names[
                                                                                                            k],
                                                                                                        black_pawns,
                                                                                                        white_figures_with_possible_beatings,
                                                                                                        should_continue)
            if board[row][col] == " O ":
                upright_squares, downright_squares, upleft_squares, downleft_squares = possible_squares_across(row, col)
                possible_squares_list = [upright_squares, downright_squares, upleft_squares,
                                         downleft_squares]
                should_continue = True
                for k in range(number_of_directions):
                    white_figures_with_possible_beatings, should_continue = check_possible_beating_queen(board, row,
                                                                                                         col,
                                                                                                         directions_names[
                                                                                                             k],
                                                                                                         black_pawns, white_pawns,
                                                                                                         white_figures_with_possible_beatings,
                                                                                                         possible_squares_list[
                                                                                                             k],
                                                                                                         should_continue)
            if board[row][col] == " x ":
                should_continue = True
                for k in range(number_of_directions):
                    black_figures_with_possible_beatings, should_continue = check_possible_beating_pawn(
                        board, row, col, directions_names[k], white_pawns,
                        black_figures_with_possible_beatings,
                        should_continue)
            if board[row][col] == " X ":
                upright_squares, downright_squares, upleft_squares, downleft_squares = possible_squares_across(
                    row, col)
                possible_squares_list = [upright_squares, downright_squares, upleft_squares,
                                         downleft_squares]
                should_continue = True
                for k in range(number_of_directions):
                    black_figures_with_possible_beatings, should_continue = check_possible_beating_queen(
                        board, row, col, directions_names[k], white_pawns, black_pawns,
                        black_figures_with_possible_beatings, possible_squares_list[k],
                        should_continue)
    return white_figures_with_possible_beatings, black_figures_with_possible_beatings

def figures_with_possible_moves(board):
    white_figures_with_possible_moves = []
    black_figures_with_possible_moves = []
    for row in range(0, 8):
        for col in range(0, 8):
            if board[row][col] == " x ":
                black_figures_with_possible_moves = check_possible_moves_pawn(board, row, col, board[row][col],
                                                                              black_figures_with_possible_moves)
            if board[row][col] == " o ":
                white_figures_with_possible_moves = check_possible_moves_pawn(board, row, col, board[row][col],
                                                                              white_figures_with_possible_moves)
            if board[row][col] == " X ":
                upright_squares, downright_squares, upleft_squares, downleft_squares = possible_squares_across(
                    row, col)
                possible_squares_list = [upright_squares, downright_squares, upleft_squares,
                                         downleft_squares]
                should_continue = True
                for k in range(number_of_directions):
                    black_figures_with_possible_moves, should_continue = check_possible_moves_queen(board, row, col,
                                                                                                    directions_names[k],
                                                                                                    black_figures_with_possible_moves,
                                                                                                    possible_squares_list[
                                                                                                        k],
                                                                                                    should_continue)
            if board[row][col] == " O ":
                upright_squares, downright_squares, upleft_squares, downleft_squares = possible_squares_across(
                    row, col)
                possible_squares_list = [upright_squares, downright_squares, upleft_squares,
                                         downleft_squares]
                should_continue = True
                for k in range(number_of_directions):
                    white_figures_with_possible_moves, should_continue = check_possible_moves_queen(board, row, col,
                                                                                                    directions_names[k],
                                                                                                    white_figures_with_possible_moves,
                                                                                                    possible_squares_list[
                                                                                                        k],
                                                                                                    should_continue)
    return white_figures_with_possible_moves, black_figures_with_possible_moves

continous_beating = 1
game = True
turn = "white"
while game == True:
    if turn == "white":
        continous_beating = 1
        print_board(board)
        time.sleep(1)
        print(f"Alive white pawns: {number_of_alive_o} Alive black pawns: {number_of_alive_x}")
        time.sleep(1)
        white_figures_with_possible_beatings, black_figures_with_possible_beatings = figures_with_possible_beatings(
            board)
        if white_figures_with_possible_beatings != []:
            white_possible_beatings = []
            white_queen_possible_beatings = []
            print("Beating required!")
            time.sleep(1)
            possible_beatings_for_player = converter_for_player(white_figures_with_possible_beatings)
            print(f"Figures with possible beatings: {possible_beatings_for_player}")
            time.sleep(1)
            chosen_figure = input("Chose a figure to beat with: ")
            chosen_figure_x, chosen_figure_y = converter_for_computer(chosen_figure)
            while continous_beating == 1:
                if chosen_figure in possible_beatings_for_player:
                    if board[chosen_figure_x][chosen_figure_y] == " o ":
                        for k in range(number_of_directions):
                            white_possible_moves = check_possible_beatings_for_pawn(board, chosen_figure_x,
                                                                                    chosen_figure_y,
                                                                                    directions_names[k],
                                                                                    black_pawns,
                                                                                    white_possible_beatings)
                        possible_moves_for_player = converter_for_player(white_possible_beatings)
                        print(f"Possible moves for chosen figure: {possible_moves_for_player}")
                        time.sleep(1)
                        chosen_move = input("Chose where do you want to move: ")
                        if chosen_move in possible_moves_for_player:
                            chosen_move_x, chosen_move_y = converter_for_computer(chosen_move)
                            direction = set_direction(chosen_figure_x, chosen_figure_y, chosen_move_x,
                                                      chosen_move_y)
                            number_of_squares = set_number_of_squares(chosen_figure_x, chosen_move_x)
                            board, number_of_alive_o, number_of_alive_x = beating(number_of_alive_o,
                                                                                  number_of_alive_x,
                                                                                  direction, board, " o ",
                                                                                  number_of_squares,
                                                                                  chosen_figure_x,
                                                                                  chosen_figure_y)
                            game_ending = check_game_winner(number_of_alive_o, number_of_alive_x)
                            queen_converter(board)
                            if game_ending == True:
                                game = False
                                break
                            chosen_figure_x = chosen_move_x
                            chosen_figure_y = chosen_move_y
                            white_possible_beatings = []
                            for k in range(number_of_directions):
                                white_possible_beatings = check_possible_beatings_for_pawn(board, chosen_figure_x,
                                                                                           chosen_figure_y,
                                                                                           directions_names[k],
                                                                                           black_pawns,
                                                                                           white_possible_beatings)
                            if white_possible_beatings == []:
                                continous_beating = 0
                                turn = "black"
                                continue
                            else:
                                white_possible_beatings = []
                                print_board(board)
                                time.sleep(1)
                                print("Another beating possible!")
                                time.sleep(1)
                                pass
                    if board[chosen_figure_x][chosen_figure_y] == " O ":
                        upright_squares, downright_squares, upleft_squares, downleft_squares = possible_squares_across(
                            chosen_figure_x, chosen_figure_y)
                        possible_squares_list = [upright_squares, downright_squares, upleft_squares,
                                                 downleft_squares]
                        for k in range(number_of_directions):
                            white_possible_beatings = check_possible_beatings_for_queen(board, chosen_figure_x,
                                                                                        chosen_figure_y,
                                                                                        directions_names[k],
                                                                                        black_pawns, white_pawns,
                                                                                        white_possible_beatings,
                                                                                        possible_squares_list[k])
                        possible_moves_for_player = converter_for_player(white_possible_beatings)
                        print(f"Possible moves for chosen figure: {possible_moves_for_player}")
                        time.sleep(1)
                        chosen_move = input("Chose where do you want to move: ")
                        if chosen_move in possible_moves_for_player:
                            chosen_move_x, chosen_move_y = converter_for_computer(chosen_move)
                            direction = set_direction(chosen_figure_x, chosen_figure_y, chosen_move_x, chosen_move_y)
                            number_of_squares = set_number_of_squares(chosen_figure_x, chosen_move_x)
                            board, number_of_alive_o, number_of_alive_x = beating(number_of_alive_o, number_of_alive_x,
                                                                                  direction, board, " O ",
                                                                                  number_of_squares, chosen_figure_x,
                                                                                  chosen_figure_y)
                            game_ending = check_game_winner(number_of_alive_o, number_of_alive_x)
                            if game_ending == True:
                                game = False
                                break
                            chosen_figure_x = chosen_move_x
                            chosen_figure_y = chosen_move_y
                            white_possible_beatings = []
                            upright_squares, downright_squares, upleft_squares, downleft_squares = possible_squares_across(
                                chosen_figure_x, chosen_figure_y)
                            possible_squares_list = [upright_squares, downright_squares, upleft_squares,
                                                     downleft_squares]
                            for k in range(number_of_directions):
                                white_possible_beatings = check_possible_beatings_for_queen(board, chosen_figure_x,
                                                                                            chosen_figure_y,
                                                                                            directions_names[k],
                                                                                            black_pawns, white_pawns,
                                                                                            white_possible_beatings,
                                                                                            possible_squares_list[k])
                            if white_possible_beatings == []:
                                continous_beating = 0
                                turn = "black"
                                continue
                            else:
                                white_possible_beatings = []
                                print_board(board)
                                time.sleep(1)
                                print("Another beating possible!")
                                time.sleep(1)
                                pass
        else:
            white_possible_moves = []
            white_queen_possible_moves = []
            print("No beatings available!")
            time.sleep(1)
            white_figures_with_possible_moves, black_figures_with_possible_moves = figures_with_possible_moves(board)
            possible_figures_for_player = converter_for_player(white_figures_with_possible_moves)
            print(f"Figures with possible moves: {possible_figures_for_player}")
            time.sleep(1)
            chosen_figure = input("Chose a figure to move: ")
            time.sleep(1)
            if chosen_figure in possible_figures_for_player:
                chosen_figure_x, chosen_figure_y = converter_for_computer(chosen_figure)
                if board[chosen_figure_x][chosen_figure_y] == " o ":
                    white_possible_moves = check_possible_moves_for_pawn(board, chosen_figure_x, chosen_figure_y, " o ",
                                                                         white_possible_moves)
                    possible_moves_for_player = converter_for_player(white_possible_moves)
                    print(f"Possible moves for chosen figure: {possible_moves_for_player}")
                    time.sleep(1)
                    chosen_move = input("Chose where do you want to move: ")
                    time.sleep(1)
                    if chosen_move in possible_moves_for_player:
                        chosen_move_x, chosen_move_y = converter_for_computer(chosen_move)
                        board = move(board, chosen_figure_x, chosen_figure_y, chosen_move_x, chosen_move_y, " o ")
                        queen_converter(board)
                        turn = "black"
                    else:
                        print("Wrong input!")
                        time.sleep(1)
                        continue
                if board[chosen_figure_x][chosen_figure_y] == " O ":
                    upright_squares, downright_squares, upleft_squares, downleft_squares = possible_squares_across(
                        chosen_figure_x, chosen_figure_y)
                    possible_squares_list = [upright_squares, downright_squares, upleft_squares,
                                             downleft_squares]
                    for k in range(number_of_directions):
                        white_queen_possible_moves = check_possible_moves_for_queen(board, chosen_figure_x,
                                                                                    chosen_figure_y,
                                                                                    directions_names[k],
                                                                                    white_queen_possible_moves,
                                                                                    possible_squares_list[k])
                    possible_moves_for_player = converter_for_player(white_queen_possible_moves)
                    print(f"Possible moves for chosen figure: {possible_moves_for_player}")
                    time.sleep(1)
                    chosen_move = input("Chose where do you want to move: ")
                    time.sleep(1)
                    if chosen_move in possible_moves_for_player:
                        chosen_move_x, chosen_move_y = converter_for_computer(chosen_move)
                        board = move(board, chosen_figure_x, chosen_figure_y, chosen_move_x, chosen_move_y, " O ")
                        turn = "black"
                    else:
                        print("Wrong input!")
                        time.sleep(1)
                        continue
            else:
                print("Wrong input!")
                time.sleep(1)
                continue

    if turn == "black":
        continous_beating = 1
        print_board(board)
        time.sleep(1)
        print(f"Alive white pawns: {number_of_alive_o} Alive black pawns: {number_of_alive_x}")
        time.sleep(1)
        white_figures_with_possible_beatings, black_figures_with_possible_beatings = figures_with_possible_beatings(
            board)
        if black_figures_with_possible_beatings != []:
            black_possible_beatings = []
            black_queen_possible_beatings = []
            print("Black has a required beating!")
            time.sleep(1)
            possible_beatings_for_player = converter_for_player(black_figures_with_possible_beatings)
            random_move = random.randint(0, (len(black_figures_with_possible_beatings) - 1))
            chosen_figure = possible_beatings_for_player[random_move]
            chosen_figure_x, chosen_figure_y = converter_for_computer(chosen_figure)
            if chosen_figure in possible_beatings_for_player:
                print(f"Black choses {chosen_figure}")
                time.sleep(1)
                while continous_beating == 1:
                    if board[chosen_figure_x][chosen_figure_y] == " x ":
                        black_possible_beatings = []
                        for k in range(number_of_directions):
                            black_possible_beatings = check_possible_beatings_for_pawn(board, chosen_figure_x,
                                                                                       chosen_figure_y,
                                                                                       directions_names[k], white_pawns,
                                                                                       black_possible_beatings)
                        possible_moves_for_player = converter_for_player(black_possible_beatings)
                        random_move = random.randint(0, (len(possible_moves_for_player) - 1))
                        chosen_move = possible_moves_for_player[random_move]
                        if chosen_move in possible_moves_for_player:
                            print(f"Black goes {chosen_move}")
                            time.sleep(1)
                            chosen_move_x, chosen_move_y = converter_for_computer(chosen_move)
                            direction = set_direction(chosen_figure_x, chosen_figure_y, chosen_move_x, chosen_move_y)
                            number_of_squares = set_number_of_squares(chosen_figure_x, chosen_move_x)
                            board, number_of_alive_o, number_of_alive_x = beating(number_of_alive_o, number_of_alive_x,
                                                                                  direction, board, " x ",
                                                                                  number_of_squares, chosen_figure_x,
                                                                                  chosen_figure_y)
                            game_ending = check_game_winner(number_of_alive_o, number_of_alive_x)
                            queen_converter(board)
                            if game_ending == True:
                                game = False
                                break
                            chosen_figure_x = chosen_move_x
                            chosen_figure_y = chosen_move_y
                            black_possible_beatings = []
                            for k in range(number_of_directions):
                                black_possible_beatings = check_possible_beatings_for_pawn(board, chosen_figure_x,chosen_figure_y,directions_names[k],white_pawns,black_queen_possible_beatings)
                            if black_possible_beatings == []:
                                continous_beating = 0
                                turn = "white"
                                continue
                            else:
                                black_possible_beatings = []
                                print_board(board)
                                time.sleep(1)
                                print("Another beating possible!")
                                time.sleep(1)
                                pass
                    if board[chosen_figure_x][chosen_figure_y] == " X ":
                        black_queen_possible_beatings = []
                        upright_squares, downright_squares, upleft_squares, downleft_squares = possible_squares_across(
                            chosen_figure_x, chosen_figure_y)
                        possible_squares_list = [upright_squares, downright_squares, upleft_squares,
                                                 downleft_squares]
                        for k in range(number_of_directions):
                            black_queen_possible_beatings = check_possible_beatings_for_queen(board, chosen_figure_x,
                                                                                     chosen_figure_y, directions_names[k],
                                                                                     white_pawns, black_pawns, black_queen_possible_beatings,
                                                                                     possible_squares_list[k])
                        possible_moves_for_player = converter_for_player(black_queen_possible_beatings)
                        random_move = random.randint(0, len(possible_moves_for_player))
                        chosen_move = possible_moves_for_player[random_move]
                        if chosen_move in possible_moves_for_player:
                            print(f"Black goes {chosen_move}")
                            time.sleep(1)
                            chosen_move_x, chosen_move_y = converter_for_computer(chosen_move)
                            direction = set_direction(chosen_figure_x, chosen_figure_y, chosen_move_x, chosen_move_y)
                            number_of_squares = set_number_of_squares(chosen_figure_x, chosen_move_x)
                            board, number_of_alive_o, number_of_alive_x = beating(number_of_alive_o, number_of_alive_x,
                                                                                  direction, board, " X ",
                                                                                  number_of_squares, chosen_figure_x,
                                                                                  chosen_figure_y)
                            game_ending = check_game_winner(number_of_alive_o, number_of_alive_x)
                            if game_ending == True:
                                game = False
                                break
                            chosen_figure_x = chosen_move_x
                            chosen_figure_y = chosen_move_y
                            black_queen_possible_beatings = []
                            upright_squares, downright_squares, upleft_squares, downleft_squares = possible_squares_across(
                                chosen_figure_x, chosen_figure_y)
                            possible_squares_list = [upright_squares, downright_squares, upleft_squares,
                                                     downleft_squares]
                            for k in range(number_of_directions):
                                black_queen_possible_beatings = check_possible_beatings_for_queen(board, chosen_figure_x,
                                                                                               chosen_figure_y,
                                                                                               directions_names[k],
                                                                                               white_pawns, black_pawns,
                                                                                               black_queen_possible_beatings,
                                                                                               possible_squares_list[k])
                            if black_queen_possible_beatings == []:
                                continous_beating = 0
                                turn = "white"
                                continue
                            else:
                                black_queen_possible_beatings = []
                                print_board(board)
                                time.sleep(1)
                                print("Another beating possible!")
                                time.sleep(1)
                                pass
        else:
            black_possible_moves = []
            black_queen_possible_moves = []
            print("No beatings for black avaiable!")
            time.sleep(1)
            white_figures_with_possible_moves, black_figures_with_possible_moves = figures_with_possible_moves(board)
            possible_figures_for_player = converter_for_player(black_figures_with_possible_moves)
            random_move = random.randint(0, (len(black_figures_with_possible_moves) - 1))
            chosen_figure = possible_figures_for_player[random_move]
            print(f"Black choses {chosen_figure}")
            time.sleep(1)
            if chosen_figure in possible_figures_for_player:
                chosen_figure_x, chosen_figure_y = converter_for_computer(chosen_figure)
                if board[chosen_figure_x][chosen_figure_y] == " x ":
                    black_possible_moves = check_possible_moves_for_pawn(board, chosen_figure_x, chosen_figure_y, " x ",
                                                                         black_possible_moves)
                    possible_moves_for_player = converter_for_player(black_possible_moves)
                    random_move = random.randint(0, (len(possible_moves_for_player) - 1))
                    chosen_move = possible_moves_for_player[random_move]
                    if chosen_move in possible_moves_for_player:
                        print(f"Black goes {chosen_move}")
                        time.sleep(1)
                        chosen_move_x, chosen_move_y = converter_for_computer(chosen_move)
                        board = move(board, chosen_figure_x, chosen_figure_y, chosen_move_x, chosen_move_y, " x ")
                        queen_converter(board)
                        turn = "white"
                    else:
                        print("Wrong input!")
                        continue
                if board[chosen_figure_x][chosen_figure_y] == " X ":
                    upright_squares, downright_squares, upleft_squares, downleft_squares = possible_squares_across(
                        chosen_figure_x, chosen_figure_y)
                    possible_squares_list = [upright_squares, downright_squares, upleft_squares,
                                             downleft_squares]
                    for k in range(number_of_directions):
                        black_queen_possible_moves = check_possible_moves_for_queen(board, chosen_figure_x,
                                                                                    chosen_figure_y,
                                                                                    directions_names[k],
                                                                                    black_queen_possible_moves,
                                                                                    possible_squares_list[k])
                    possible_moves_for_player = converter_for_player(black_queen_possible_moves)
                    random_move = random.randint(0, (len(possible_moves_for_player) - 1))
                    chosen_move = possible_moves_for_player[random_move]
                    if chosen_move in possible_moves_for_player:
                        print(f"Black goes {chosen_move}")
                        chosen_move_x, chosen_move_y = converter_for_computer(chosen_move)
                        board = move(board, chosen_figure_x, chosen_figure_y, chosen_move_x, chosen_move_y, " X ")
                        turn = "white"
                    else:
                        print("Wrong input!")
                        continue
