number_of_elements = int(input())

set_of_elements = set(map(int, input().split()))

number_of_commands = int(input())

for i in range(number_of_commands):
    print(set_of_elements)
    command = input()
    split_command = command.split()
    if split_command[0] == "pop":
        set_of_elements.pop()
    if split_command[0] == "discard":
        set_of_elements.discard(int(split_command[1]))
    if split_command[0] == "remove":
        try:
            set_of_elements.remove(int(split_command[1]))
        except KeyError:
            pass
        
print(sum(set_of_elements))
    
