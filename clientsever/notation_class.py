import json

class Notation:
    def __init__(self):
        pass

    def encode_json(self, entry):
        entry_json = json.dumps(entry)
        return entry_json

    def decode_json(self, entry_json):
        entry = json.loads(entry_json)
        return entry
