import random
import tkinter as tk
from tkinter.filedialog import askopenfilename, asksaveasfilename
import easygui as gui

window = tk.Tk()

window.title("Make your own poem!")

def generate():
    nouns = nouns_input.get()
    verbs = verbs_input.get()
    adjectives = adjectives_input.get()
    prepositions = prepositions_input.get()
    adverbs = adverbs_input.get()
    def process_word_type(word_type):
        word_list = []
        helpful_variable = 0
        helpful_variable_2 = 0 
        for i in range(len(word_type)):
            if word_type[i] == "," and helpful_variable == 0:
                word_list.append(word_type[0:i])
                helpful_variable_2 = i+1
                helpful_variable = 1
                continue
            if word_type[i] == "," and helpful_variable == 1:
                word_list.append(word_type[helpful_variable_2:i])
                helpful_variable_2 = i+1
                helpful_variable = 1
            if i+1 == len(word_type):
                word_list.append(word_type[helpful_variable_2:i+1])
                helpful_variable_2 = 0
                helpful_variable = 0
        return word_list

    nouns_list = process_word_type(nouns)
    verbs_list = process_word_type(verbs)
    adjectives_list = process_word_type(adjectives)
    prepositions_list = process_word_type(prepositions)
    adverbs_list = process_word_type(adverbs)
    print(nouns_list)
    print(verbs_list)
    print(adjectives_list)
    print(prepositions_list)
    print(adverbs_list)
    if len(nouns_list) < 3 or len(verbs_list) < 3 or len(adjectives_list) < 3 or len(prepositions_list) < 3 or len(adverbs_list) < 1:
        gui.msgbox(msg = "The poem needs to have at least: \n three nouns \n three verbs \n three adjectives \n three prepositions \n one adverb \n Please refill the blanks and try again!", title= "Error!")
    else:
        def select_random_words(word_list):
            final_word_list = []
            for i in range (3):
                random_number = random.randint(0, len(word_list)-1)
                final_word_list.append(word_list[random_number])
                word_list.remove(word_list[random_number])
            return final_word_list
        final_nouns_list = select_random_words(nouns_list)
        final_verbs_list = select_random_words(verbs_list)
        final_adjectives_list = select_random_words(adjectives_list)
        final_prepositions_list = select_random_words(prepositions_list)
        random_number = random.randint(0, len(adverbs_list)-1)
        final_adverb = adverbs_list[random_number]
        prefix = "A"
        if final_adjectives_list[0][0].lower() in ["a", "e", "i", "o", "u"]:
            prefix = "An"
        generated_text["text"] = f"{prefix} {final_adjectives_list[0]} {final_nouns_list[0]} \n \n A {final_adjectives_list[0]} {final_nouns_list[0]} {final_verbs_list[0]} {final_prepositions_list[0]} the {final_adjectives_list[1]} {final_nouns_list[1]} \n {final_adverb}, the {final_nouns_list[0]}, {final_verbs_list[1]} \n the {final_nouns_list[1]} {final_verbs_list[2]} {final_prepositions_list[1]} a {final_adjectives_list[2]} {final_nouns_list[2]}" 
                                       
        
    
    
def save_file():
    filepath = asksaveasfilename(
        defaultextension="txt",
        filetypes=[("Text Files", "*.txt"), ("All Files", "*.*")],
        )
    if not filepath:
        return

    with open(filepath, "w") as output_file:
        text=generated_text.cget("text")
        output_file.write(text)

    window.title(f"Poem - {filepath}")

window.rowconfigure(0, minsize = 30, weight=1)
window.rowconfigure([1, 2, 3, 4, 5], minsize = 20, weight = 1)
window.rowconfigure(6, minsize = 30, weight = 1)
window.rowconfigure(7, minsize = 100, weight = 1)
window.rowconfigure(8, minsize = 30, weight = 1)
window.columnconfigure(1, minsize=200, weight=1)

top_message = tk.Label(text = "Enter your favorite words, separated by commas.")
top_message.grid(row = 0, column = 1, stick = "nesw")

nouns_message = tk.Label(text = "Nouns:")
nouns_message.grid(row = 1, column = 0, stick = "e")
nouns_input = tk.Entry(width=40)
nouns_input.grid(row = 1, column = 1)

verbs_message = tk.Label(text = "Verbs:")
verbs_message.grid(row = 2, column = 0, stick = "e")
verbs_input = tk.Entry(width=40)
verbs_input.grid(row = 2, column = 1)

adjectives_message = tk.Label(text = "Adjectives:")
adjectives_message.grid(row = 3, column = 0, stick = "e")
adjectives_input = tk.Entry(width=40)
adjectives_input.grid(row = 3, column = 1)

prepositions_message = tk.Label(text = "Prepositions:")
prepositions_message.grid(row = 4, column = 0, stick = "e")
prepositions_input = tk.Entry(width=40)
prepositions_input.grid(row = 4, column = 1)

adverbs_message = tk.Label(text = "Adverbs:")
adverbs_message.grid(row = 5, column = 0, stick = "e")
adverbs_input = tk.Entry(width=40)
adverbs_input.grid(row = 5, column = 1)

generate_button = tk.Button(text= "Generate", command = generate)
generate_button.grid(row = 6, column = 1)

generated_text = tk.Label()
generated_text.grid(row = 7, column = 1)

save_file_button = tk.Button(text= "Save File", command = save_file)
save_file_button.grid(row = 8, column = 1)

window.mainloop()
