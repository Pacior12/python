import random
import tkinter as tk

window = tk.Tk()


def change():
    number = random.randint(1, 6)
    lbl_value["text"] = f"{number}"
    
    
    

window.rowconfigure([0, 1], minsize=50, weight=1)
window.columnconfigure(0, minsize=50, weight=1)

btn_clickme = tk.Button(master=window, text = "Click Me!", command = change)
btn_clickme.grid(row = 0, column = 0, sticky = "nsew")

lbl_value = tk.Label(master=window)
lbl_value.grid(row=1, column = 0)

window.mainloop()
