import time
import psycopg2

conn = psycopg2.connect(host = "localhost", dbname = "postgres", user = "postgres", password = "1234", port = 5432)
cur = conn.cursor()

cur.execute("""CREATE TABLE IF NOT EXISTS test (
                name VARCHAR(255),
                password VARCHAR(255)
                )""")

cur.execute("""INSERT INTO test(name, password) VALUES (%s, %s)""", ("test", "1234"))
conn.commit()
cur.close()
conn.close()

t1 = time.perf_counter()

for _ in range(10000):
    conn = psycopg2.connect(host = "localhost", dbname = "postgres", user = "postgres", password = "1234", port = 5432)
    cur = conn.cursor()
    cur.execute(""" SELECT name FROM test WHERE name = %s""", ("test",))
    cur.close()
    conn.close()
    
t2 = time.perf_counter()

print(f"Process 1 finished in {t2-t1} seconds!")



