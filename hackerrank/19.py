from collections import OrderedDict
od=OrderedDict()
n=int(input())
for _ in range(n):
    name,price=input().rsplit(" ",1)
    str1=''.join(name)
    if str1 in od:
        od[str1]+=int(price)
    else:
        od[str1]=int(price)

for key, value in od.items():
    print(f"{key} {value}")
