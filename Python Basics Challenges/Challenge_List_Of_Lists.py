universities = [
['California Institute of Technology', 2175, 37704],
['Harvard', 19627, 39849],
['Massachusetts Institute of Technology', 10566, 40732],
['Princeton', 7802, 37000],
['Rice', 5879, 35551],
['Stanford', 19535, 40569],
['Yale', 11701, 40500]
]

def enrollment_stats(universities):
    numbers_of_students = []
    numbers_of_enrollment_values = []
    for n in range(len(universities)):
        numbers_of_students.append(universities[n][1])
        numbers_of_enrollment_values.append(universities[n][2])
    return numbers_of_students, numbers_of_enrollment_values
    
number_of_students, number_of_enrollments = enrollment_stats(universities)
sum_of_enrollments = sum(number_of_enrollments)
sum_of_students = sum(number_of_students)



def mean(sum_of_enrollments, sum_of_students, universities):
    student_mean = sum_of_students/len(universities)
    tuition_mean = sum_of_enrollments/len(universities)
    return student_mean, tuition_mean

mean_students, mean_tuition = mean(sum_of_enrollments, sum_of_students, universities)

def median(number_of_students, number_of_enrollments):
    number_of_students.sort()
    number_of_enrollments.sort()
    median_of_students = number_of_students[3]
    median_of_enrollments = number_of_enrollments[3]
    return median_of_students, median_of_enrollments

median_students, median_tuition = median(number_of_students, number_of_enrollments)

print("******************************")
print(f"Total students:   {sum_of_students:,}")
print(f"Total tuition:   ${sum_of_enrollments:,}")
print(" ")
print(f"Student mean:     {mean_students:,.2f}")
print(f"Student median:   {median_students:,}")
print("")
print(f"Tuition mean:    ${mean_tuition:,.2f}")
print(f"Tuition median:  ${median_tuition:,}")
print("******************************")
