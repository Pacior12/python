import socket as s
import json
import time
import psycopg2
from database_class import Database
from notation_class import Notation

class Server:
    def __init__(self, host, port, buffer, database, notation, max_number_of_messages, max_database_string_length):
        self.host = host
        self.port = port
        self.buffer = buffer
        self.logged = False
        self.cur = None
        self.conn = None
        self.database = database
        self.notation = notation
        self.max_number_of_messages = max_number_of_messages
        self.max_database_string_length = max_database_string_length

    def connect(self):
        self.server_socket = s.socket(s.AF_INET, s.SOCK_STREAM)
        self.server_socket.bind((self.host, self.port))
        self.server_socket.listen(2)
        self.client_socket, self.address = self.server_socket.accept()

    def send_initial_message(self):
        msg = "Choose wherther to login or register".encode("utf8")
        self.client_socket.send(msg)

    def get_messages(self, login):
        messages = {}
        number_of_messages = self.database.select_data_by_name("number_of_users_messages", login)
        if int(number_of_messages) > 0:
            for message_index in range(self.max_number_of_messages):
                message = self.database.select_data_by_name(f"msg{message_index}", login)
                if message is not None:
                    sender = self.database.select_data_by_name(f"msg{message_index}_sender", login)
                    messages[f"{message_index + 1}. {sender}"] = message # so that the list goes from 1-5 not from 0-4
                    response = messages
                    self.database.update_data_by_name(f"msg{message_index}", None, login)
                    self.database.update_data_by_name(f"msg{message_index}_sender", None, login)
                    self.database.update_data_by_name("number_of_users_messages", "0", login)
        else:
            response = "Messagebox empty!"
        return response

    def get_info(self, admin_status):
        response = {}
        response['"info"'] = "gives user the list of available commands"
        response['"quit"'] = "quits the application"
        response['"message" "username" "text"'] = "sends a message to other user"
        response['"read_message"'] = "reads messages in user's inbox"
        response['"version"'] = "gives user the current version of the app"
        response['"my_data"'] = "give user all of his data"
        if admin_status is True:
            response['"make_admin" "username"'] = "makes user an admin"
            response['"delete_admin" "username"'] = "makes user no longer an admin"
            response['"delete_user" "username"'] = "deletes user from the database"
            response['"all_data"'] = "gives admin all the data from the database"
        return response

    def make_admin(self, split_command):
        target_user = self.database.select_data_by_name("name", split_command[1])
        if target_user is not None:
            self.database.update_data_by_name("if_admin", True, target_user)
            response = f"{target_user} is now an admin"
        else:
            response = "Incorrect username"
        return response

    def delete_admin(self, split_command):
        target_user = self.database.select_data_by_name("name", split_command[1])
        if target_user is not None:
            target_user = split_command[1]
            self.database.update_data_by_name("if_admin", False,target_user)
            response = f"{target_user} is no longer an admin"
        else:
            response = "Incorrect username"
        return response

    def delete_user(self, split_command):
        target_user = self.database.select_data_by_name("name", split_command[1])
        if target_user is not None:
            self.database.delete_user_by_name(target_user)
            response = f"{target_user} has been deleted"
        else:
            response = "Incorrect username"
        return response
        

    def send_user_message(self, split_command, login):
        target_user = split_command[1]
        user_in_the_database = self.database.select_data_by_name("name", target_user)
        if user_in_the_database is not None:
            del split_command[:2]  # deleting username and command from the list
            message = ' '.join(split_command)
            if len(message) < self.max_database_string_length:
                for message_index in range(self.max_number_of_messages):
                    message_result = self.database.select_data_by_name(f"msg{message_index}", target_user)
                    if message_result is None:
                        self.database.update_data_by_name(f"msg{message_index}", message, target_user)
                        self.database.update_data_by_name(f"msg{message_index}_sender", login, target_user)
                        self.database.update_data_by_name("number_of_users_messages",f"{message_index + 1}" , target_user)
                        response = "Message delivered successfully!"
                        break
                    if message_result is not None and message_index == self.max_number_of_messages - 1: #the message_index goes to 4 and the max_number_of_messages is 5
                        response = "Recipient's message box is full!"
                        break
            else:
                response = "Message too long!"
        else:
            response = "Incorrect username"
        return response
        

    def process_login_and_return_response(self, logged, choice, login, password):
        login_result = self.database.select_data_by_name("name", login)
        if choice == "login":
            if login_result is not None:
                password_result = self.database.select_data_by_name("password", login)
                if password_result == password:
                    response = f"Successfully logged in as {login}"
                    logged = True
                else:
                    response = "Wrong password"
            else:
                response = "User does not exist"
        if choice == "register":
            if login_result is not None and len(login) < self.max_database_string_length:
                response = "User already exists!"
                pass
            else:
                if len(password) > self.max_database_string_length:
                    response = "Password too long!"
                else:
                    self.database.add_an_account(login, password, "no")
                    response = f"Successfully logged in as {login}"
                    logged = True
        return login, logged, response

    def process_command_and_return_response(self, logged, login, command):
        admin_status = self.database.select_data_by_name("if_admin", login)
        split_command = command.split()
        response = "Unknown command"
        if command == "logout":
            response = "You logged out successfully"
            logged = False
        if command == "quit":
            response = "Connection terminated"
        if command == "version":
            response = "This is a 1.1 version of this application"
        if command == "my_data":
            response = self.database.select_all_data_by_name("*", login)
        if split_command[0] == "message" and len(split_command) >= 3:
            response = self.send_user_message(split_command, login)
        if command == "info":
            response = self.get_info(admin_status)
        if command == "read_messages":
            response = self.get_messages(login)
        if admin_status is True:
            if command == "all_data":
                response = self.database.select_all_data("*")
            if split_command[0] == "make_admin" and len(split_command) == 2:
                response = self.make_admin(split_command)
            if split_command[0] == "delete_admin" and len(split_command) == 2:
                response = self.delete_admin(split_command)
            if split_command[0] == "delete_user" and len(split_command) == 2:
                response = self.delete_user(split_command)
        return logged, response
