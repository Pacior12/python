import random

def toss():
    counter = 0
    if random.randint(0, 1) == 0:
        counter = counter + 1
        while (1 > 0):
            if random.randint(0, 1) == 1:
                counter = counter + 1
                return counter
                break
            else:
                counter = counter + 1
    else:
        counter = counter + 1
        while (1 > 0):
            if random.randint(0, 1) == 0:
                counter = counter + 1
                return counter
                break
            else:
                counter = counter + 1

total_tosses = 0

for n in range(10000):
    number_of_tosses = toss()
    total_tosses = total_tosses + number_of_tosses


avg_tosses = total_tosses / 10000

print(f"The average numbers of coin tosses to get both heads and tails is {avg_tosses}")
        

    
  
