import random

Nouns = ["fossil", "horse", "aardvark", "judge", "chef", "mango",
"extrovert", "gorilla"]
Verbs = ["kicks", "jingles", "bounces", "slurps", "meows",
"explodes", "curdles"]
Adjectives = ["furry", "balding", "incredulous", "fragrant",
"exuberant", "glistening"]
Prepositions = ["against", "after", "into", "beneath", "upon",
"for", "in", "like", "over", "within"]
Adverbs = ["curiously", "extravagantly", "tantalizingly",
"furiously", "sensuously"]


chosen_nouns = [random.choice(Nouns), random.choice(Nouns), random.choice(Nouns)]
chosen_verbs = [random.choice(Verbs), random.choice(Verbs), random.choice(Verbs)]
chosen_adjectives = [random.choice(Adjectives), random.choice(Adjectives), random.choice(Adjectives)]
chosen_prepositions = [random.choice(Prepositions), random.choice(Prepositions)]
chosen_adverbs = [random.choice(Adverbs)]

while (chosen_nouns[0] == chosen_nouns[1] or chosen_nouns[1] == chosen_nouns[2] or chosen_nouns[0] == chosen_nouns[2]):
    if chosen_nouns[0] == chosen_nouns[1]:
        chosen_nouns[1] = random.choice(Nouns)
    if chosen_nouns[1] == chosen_nouns[2]:
        chosen_nouns[2] = random.choice(Nouns)
    if chosen_nouns[0] == chosen_nouns[2]:
        chosen_nouns[0] = random.choice(Nouns)
        
while (chosen_verbs[0] == chosen_verbs[1] or chosen_verbs[1] == chosen_verbs[2] or chosen_verbs[0] == chosen_verbs[2]):
    if chosen_verbs[0] == chosen_verbs[1]:
        chosen_verbs[1] = random.choice(Verbs)
    if chosen_verbs[1] == chosen_verbs[2]:
        chosen_verbs[2] = random.choice(Verbs)
    if chosen_verbs[0] == chosen_verbs[2]:
        chosen_verbs[0] = random.choice(Verbs)

while (chosen_adjectives[0] == chosen_adjectives[1] or chosen_adjectives[1] == chosen_adjectives[2] or chosen_adjectives[0] == chosen_adjectives[2]):
    if chosen_adjectives[0] == chosen_adjectives[1]:
        chosen_adjectives[1] = random.choice(Adjectives)
    if chosen_adjectives[1] == chosen_adjectives[2]:
        chosen_adjectives[2] = random.choice(Adjectives)
    if chosen_adjectives[0] == chosen_adjectives[2]:
        chosen_adjectives[0] = random.choice(Adjectives)

while (chosen_prepositions[0] == chosen_prepositions[1]):
    if chosen_prepositions[0] == chosen_prepositions[1]:
        chosen_prepositions[1] = random.choice(Prepositions)
        

if ((chosen_adjectives[0][0] == "o") or (chosen_adjectives[0][0] == "a") or (chosen_adjectives[0][0] == "e") or (chosen_adjectives[0][0] == "i")):
    a_an = "An"
else:
    a_an = "A"

print(f"{a_an} {chosen_adjectives[0]} {chosen_nouns[0]}\n")
print(f"{a_an} {chosen_adjectives[0]} {chosen_nouns[0]} {chosen_verbs[0]} {chosen_prepositions[0]} the {chosen_adjectives[1]} {chosen_nouns[1]}")
print(f"{chosen_adverbs[0]}, the {chosen_nouns[0]} {chosen_verbs[1]}")
print(f"the {chosen_nouns[1]} {chosen_verbs[2]} {chosen_prepositions[1]} a {chosen_adjectives[2]} {chosen_nouns[2]}")

