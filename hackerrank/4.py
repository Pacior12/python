
from itertools import permutations
s,k = input().split()
permutations = list(permutations(s,int(k)))
for i in sorted(permutations):
    print(''.join(i))
