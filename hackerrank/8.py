test_cases = int(input())

for i in range(test_cases):
    elements_A = int(input())
    A = set(map(int, input().split()))
    elements_B = int(input())
    B = set(map(int, input().split()))
    
    if A <= B:
        print(True)
    else:
        print(False)
