class priorityQueue:
    def __init__(self, size=10):
        self.size = size
        self.my_queue = []

    def add_element(self, item, priority):
        if priority >= 0:
            self.my_queue.append(f"{item} : Priority {priority}")

    def find_first_queue_item(self):
        highest_priority = 0
        if len(self.my_queue) > 0:
            for queue_element in range(len(self.my_queue)):
                split_queue = self.my_queue[queue_element].split()
                if int(split_queue[3]) > highest_priority:
                    highest_priority = int(split_queue[3])
            for queue_element in range(len(self.my_queue)):
                split_queue = self.my_queue[queue_element].split()
                if int(split_queue[3]) == highest_priority:
                    highest_priority_item = split_queue[0]
                    return highest_priority_item
        else:
            print("queue is empty!")

    def return_first_queue_item(self):
        highest_priority_item = priorityQueue.find_first_queue_item(self)
        print(highest_priority_item)

    def take_and_delete_first_queue_item(self):
        if len(self.my_queue) > 0:
            highest_priority_item = priorityQueue.find_first_queue_item(self)
            for queue_element in range (len(self.my_queue)):
                split_queue = self.my_queue[queue_element].split()
                if split_queue[0] == highest_priority_item:
                    print(highest_priority_item)
                    self.my_queue.pop(queue_element)
                    break
        else:
            print("queue is empty!")

    def is_queue_empty(self):
        if len(self.my_queue) == 0:
            print("yes")
        else:
            print("no")

    def return_queue_length(self):
        print(len(self.my_queue))

queue = priorityQueue()

queue.add_element("Wood", 5)
queue.add_element("Stone", 7)
queue.add_element("Marble", 3)
queue.add_element("Stone", 7)
queue.add_element("Wood", 5)





