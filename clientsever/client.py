import socket as s
import json
from notation_class import Notation
from client_class import Client


HOST = "127.0.0.1"
logged = False
keep_going = True
PORT = 65432
BUFFER = 1024

notation = Notation()

client = Client(HOST, PORT, BUFFER, notation)
client.connect()
client.recieve_initial_message()

while keep_going:
    while not logged:
        logged = client.login_or_registration(logged)
    while logged:
        command = input("Enter a command:")
        command_json = notation.encode_json(command)
        client.client_socket.send(command_json.encode("utf8"))
        response_json = client.client_socket.recv(BUFFER).decode("utf-8")
        response = notation.decode_json(response_json)
        if type(response) is dict:
            for i in range(len(response)):
                response_list = list(response.keys())
                first_value = response_list[i]
                second_value = response[first_value]
                print(f"{first_value}: {second_value}")
        else:
            print(response)
            if response == "You logged out successfully":
                logged = False
            if response == "Connection terminated":
                keep_going = False
                break
                client.client_socket.close()

client.client_socket.close()
