import pygame
from sys import exit
from random import randint, choice
import time

class Player(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.image.load('graphics/player.png').convert_alpha()
        self.image = pygame.transform.rotozoom(self.image, 0, 0.5)
        self.rect = self.image.get_rect(midbottom = (-6, 250))
        

    def player_input(self):
        keys = pygame.key.get_pressed()
        if keys[pygame.K_UP] and self.rect.top >= 0:
            self.rect.y -= 6
        if keys[pygame.K_DOWN] and self.rect.bottom <= 600:
            self.rect.y += 6

    def update(self):
        self.player_input()

class Enemy(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.image.load('graphics/enemy.png').convert_alpha()
        self.image = pygame.transform.rotozoom(self.image, 0, 0.5)
        self.rect = self.image.get_rect(midbottom = (1206, 250))

    def enemy_move(self):
        if ball.sprite.rect.y > self.rect.y and self.rect.bottom < 600:
            self.rect.y += randint(0, 4)
        if ball.sprite.rect.y < self.rect.y and self.rect.top > 0:
            self.rect.y -= randint(0, 4)

    def update(self):
        self.enemy_move()
    

class Ball(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.player_score = 0
        self.enemy_score = 0
        self.image = pygame.image.load('graphics/ball.png').convert_alpha()
        self.image = pygame.transform.rotozoom(self.image, 0, 0.1)
        self.rect = self.image.get_rect(midbottom = (600, 300))
        self.number_to_change_y = randint(3, 5)
        self.randomizer_for_y = randint(1, 2)
        self.randomizer_for_x = randint(1, 2)
        self.number_to_change_x = 10
        self.hit_sound = pygame.mixer.Sound("sounds/Hitsound.wav")
        if self.randomizer_for_y == 2:
            self.number_to_change_y = 0 - self.number_to_change_y
        if self.randomizer_for_x == 2:
            self.number_to_change_x = 0 - self.number_to_change_x
            
    def scoring(self):
        if self.rect.right >= 1225:
            self.player_score += 1
            time.sleep(1)
            self.number_to_change_x = -self.number_to_change_x
            self.number_to_change_y = randint(3, 5)
            self.rect.y = 300
            self.rect.x = 600
        if self.rect.left <= -25:
            time.sleep(1)
            self.number_to_change_y = randint(2, 5)
            self.enemy_score += 1
            self.rect.y = 300
            self.rect.x = 600

    def ball_fly(self):
        self.rect.x += self.number_to_change_x
        self.rect.y += self.number_to_change_y
    
    def ball_bounce(self):
        if self.rect.top <= 0:
            self.rect.top = 0
            self.number_to_change_y = -self.number_to_change_y
            self.hit_sound.play(0)
        if self.rect.bottom >= 600:
            self.rect.bottom = 600
            self.number_to_change_y = -self.number_to_change_y
            self.hit_sound.play(0)
        if pygame.sprite.collide_rect(player.sprite, ball.sprite) == True:
            self.number_to_change_x = -self.number_to_change_x
            self.hit_sound.play(0)
            self.rect.x +=5
            self.rect.y +=5
        if pygame.sprite.collide_rect(enemy.sprite, ball.sprite) == True:
            self.number_to_change_x = -self.number_to_change_x
            self.hit_sound.play(0)
            self.rect.x -=5
            self.rect.y -=5
            
            
    def update(self):
        self.scoring()
        self.ball_fly()
        self.ball_bounce()


def display_score():
    score_surf = font.render(f"{ball.sprite.player_score}-{ball.sprite.enemy_score}", False, "#E8760B")
    score_rect = score_surf.get_rect(center = (600,50))
    screen.blit(score_surf, score_rect)
    
pygame.init()
score_player = 0
score_computer = 0
font = pygame.font.Font('font/Pixeltype.ttf', 50)
screen = pygame.display.set_mode((1200,600))
pygame.display.set_caption('Basket Pong')
clock = pygame.time.Clock()

space_jam_sound = pygame.mixer.Sound("sounds/Space Jam.mp3")
in_da_club_sound = pygame.mixer.Sound("sounds/In Da Club.mp3")
lets_get_sound = pygame.mixer.Sound("sounds/Lets Get Ready to Rumble.mp3")

player = pygame.sprite.GroupSingle()
player.add(Player())

ball = pygame.sprite.GroupSingle()
ball.add(Ball())

enemy = pygame.sprite.GroupSingle()
enemy.add(Enemy())
play = 0
instructions = 0
musicmenu = 0

game_active = False

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            exit()
            
    if game_active:
        screen.fill(("Black"))
        player.draw(screen)
        player.update()
        ball.draw(screen)
        ball.update()
        enemy.draw(screen)
        enemy.update()
        display_score()
        if ball.sprite.player_score >= 5 or ball.sprite.enemy_score >= 5:
            game_active = False
            screen.fill("Black")

            play_again_button = pygame.image.load('graphics/Play Again.png')
            play_again_button_rect = play_again_button.get_rect(center = (300, 300))

            back_button = pygame.image.load("graphics/Back.png")
            back_button_rect = back_button.get_rect(center = (900, 300))

            if ball.sprite.player_score == 5:
                second_line = font.render("The player has won!", False, "#E8760B")
            if ball.sprite.enemy_score == 5:
                second_line = font.render("The enemy has won!", False, "#E8760B")

            first_line = font.render("Game over!", False, "#E8760B")
            first_line_rect = first_line.get_rect(center = (600, 100))
            second_line_rect = first_line.get_rect(center = (550, 200))
            
            screen.blit(play_again_button, play_again_button_rect)
            screen.blit(back_button, back_button_rect)
            screen.blit(first_line, first_line_rect)
            screen.blit(second_line, second_line_rect)

            if event.type == pygame.MOUSEBUTTONDOWN:
                if play_again_button_rect.collidepoint(event.pos):
                    ball.sprite.player_score = 0
                    ball.sprite.enemy_score = 0
                    game_active = True

            if event.type == pygame.MOUSEBUTTONDOWN:
                if back_button_rect.collidepoint(event.pos):
                    play = 0
                    instructions = 0
                    musicmenu = 0
                    ball.sprite.player_score = 0
                    ball.sprite.enemy_score = 0
                    player.sprite.rect.y = 250
                    enemy.sprite.rect.y = 250
                    game_active = False
                                

            
    else:
        if play == 0 and musicmenu == 0 and instructions == 0:
            menu_background = pygame.image.load('graphics/basketPONG.png')
            menu_background_rect = menu_background.get_rect(center = (600, 300))

            play_button = pygame.image.load('graphics/Play.png')
            play_button_rect = play_button.get_rect(center = (600, 250))

            instructions_button = pygame.image.load('graphics/Instructions.png')
            instructions_button_rect = instructions_button.get_rect(center = (600, 375))

            musicmenu_button = pygame.image.load('graphics/Music menu.png')
            musicmenu_button_rect = musicmenu_button.get_rect(center = (600, 500))

            screen.blit(menu_background, menu_background_rect)
            screen.blit(play_button, play_button_rect)
            screen.blit(instructions_button, instructions_button_rect)
            screen.blit(musicmenu_button, musicmenu_button_rect)

            if event.type == pygame.MOUSEBUTTONDOWN:
                if play_button_rect.collidepoint(event.pos):
                    play = 1

            if event.type == pygame.MOUSEBUTTONDOWN:
                if instructions_button_rect.collidepoint(event.pos):
                    instructions = 1
                    
            if event.type == pygame.MOUSEBUTTONDOWN:
                if musicmenu_button_rect.collidepoint(event.pos):
                    musicmenu = 1

        if play == 0 and musicmenu == 0 and instructions == 1:
            screen.fill("Black")
            instructions_text_1 = font.render("Use your up and down arrow", False, "#E8760B")
            instructions_text_1_rect = instructions_text_1.get_rect(center = (600, 250))

            instructions_text_2 = font.render("to move your platform", False, "#E8760B")
            instructions_text_2_rect = instructions_text_2.get_rect(center = (600, 400))
            
            back_button = pygame.image.load("graphics/Back.png")
            back_button_rect = back_button.get_rect(center = (200,500))

            screen.blit(instructions_text_1, instructions_text_1_rect)
            screen.blit(instructions_text_2, instructions_text_2_rect)
            screen.blit(back_button, back_button_rect)

            if event.type == pygame.MOUSEBUTTONDOWN:
                if back_button_rect.collidepoint(event.pos):
                    instructions = 0
                    
        if play == 0 and instructions == 0 and musicmenu == 1:
            screen.fill("Black")
            
            disable_music_button = pygame.image.load("graphics/Disable Music.png")
            disable_music_button_rect = disable_music_button.get_rect(center = (600,100))
            
            space_jam_button = pygame.image.load("graphics/Space Jam.png")
            space_jam_button_rect = space_jam_button.get_rect(center = (200,300))
            
            lets_get_button = pygame.image.load("graphics/Lets get ready to rumble.png")
            lets_get_button_rect = lets_get_button.get_rect(center = (600,300))
            
            in_da_club_button = pygame.image.load("graphics/In Da Club.png")
            in_da_club_button_rect = in_da_club_button.get_rect(center = (1000, 300))

            back_button = pygame.image.load("graphics/Back.png")
            back_button_rect = back_button.get_rect(center = (200,500))

            screen.blit(disable_music_button, disable_music_button_rect)
            screen.blit(space_jam_button, space_jam_button_rect)
            screen.blit(lets_get_button, lets_get_button_rect)
            screen.blit(in_da_club_button, in_da_club_button_rect)
            screen.blit(back_button, back_button_rect)

            if event.type == pygame.MOUSEBUTTONDOWN:
                if disable_music_button_rect.collidepoint(event.pos):
                    space_jam_sound.stop()
                    in_da_club_sound.stop()
                    lets_get_sound.stop()

            if event.type == pygame.MOUSEBUTTONDOWN:
                if space_jam_button_rect.collidepoint(event.pos):
                    space_jam_sound.play(-1)
                    in_da_club_sound.stop()
                    lets_get_sound.stop()

                    time.sleep(1)

            if event.type == pygame.MOUSEBUTTONDOWN:
                if lets_get_button_rect.collidepoint(event.pos):
                    lets_get_sound.play(-1)
                    space_jam_sound.stop()
                    in_da_club_sound.stop()
                    time.sleep(1)

            if event.type == pygame.MOUSEBUTTONDOWN:
                if in_da_club_button_rect.collidepoint(event.pos):
                    in_da_club_sound.play(-1)
                    lets_get_sound.stop()
                    space_jam_sound.stop()
                    time.sleep(1)

            if event.type == pygame.MOUSEBUTTONDOWN:
                if back_button_rect.collidepoint(event.pos):
                    musicmenu = 0
            
        if play == 1:
            game_active = True
        
    pygame.display.update()
    clock.tick(60)
