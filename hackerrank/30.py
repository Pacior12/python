
valid_first_digits = ["7","8","9"]

valid_length_number = 10

test_cases = int(input())

for test_case in range(test_cases):
    case = input()
    if case[0] in valid_first_digits and len(case) == valid_length_number and case.isnumeric() == True:
        print("YES")
    else:
        print("NO")
