import easygui as gui
from PyPDF2 import PdfReader, PdfWriter

input_path = gui.fileopenbox(
    title="Select a PDF  to cut",
    default = "*pdf"
    )
if input_path is None:
    exit()

input_file = PdfReader(input_path)

number_of_pages = len(input_file.pages)

first_page = gui.enterbox(
    msg=f"What page you want to start from? Number of pages: {number_of_pages}",
    title= "Select first page",
    )
first_page = int(first_page)

if first_page < 0 or first_page > number_of_pages:
    exit()

last_page = gui.enterbox(
    msg=f"What page you want to end at? Range of pages: {first_page} - {number_of_pages}",
    title= "Select last page",
    )

last_page = int(last_page)

if last_page < first_page or last_page > number_of_pages:
    exit()

save_title = "Save the rotated PDF as ..."
file_type = "*.pdf"
output_path = gui.filesavebox(title=save_title, default=file_type)

while input_path == output_path:
    gui.msgbox(msg ="Cannot overwrite the original file!")
    output_path = gui.filesavebox(title=save_title, default=file_type)

if output_path is None:
    exit()

output_pdf = PdfWriter()

for page in range (first_page, last_page):
    output_pdf.add_page(input_file.pages[page])

with open(output_path, "wb") as output_file:
    output_pdf.write(output_file)

gui.msgbox(msg= "Your file has been split succesfully!", title = "Success!")
