import socket as s
import json
from notation_class import Notation

class Client:
    def __init__(self, host, port, buffer, notation):
        self.host = host
        self.port = port
        self.buffer = buffer
        self.notation = notation

    def connect(self):
        self.client_socket = s.socket(s.AF_INET, s.SOCK_STREAM)
        self.client_socket.connect((self.host, self.port))

    def recieve_initial_message(self):
        print(self.client_socket.recv(self.buffer).decode("utf8"))

    def login_or_registration(self, logged):
        while True:
            choice = input("Choose whether to login or register: ")
            if choice in ("login", "register"):
                login = input("Enter your login: ")
                password = input("Enter your password: ")
                login_data = {
                    'choice': choice,
                    'login': login,
                    'password': password
                }
                combined_login_data = self.notation.encode_json(login_data)
                self.client_socket.send(combined_login_data.encode("utf8"))
                combined_initial_message = self.client_socket.recv(self.buffer).decode("utf-8")
                initial_message = self.notation.decode_json(combined_initial_message)
                response_key, response_value = list(initial_message.items())[0]
                if response_key == 'response' and response_value == f"Successfully logged in as {login}":
                    print(initial_message['response'])
                    print(initial_message['info_message'])
                    if initial_message['messages_message'] != "You have 0 messages in your mailbox!":
                        print(initial_message['messages_message'])
                        print(initial_message['read_messages'])
                    logged = True
                    break
                else:
                    print(response_value)
            else:
                print("Wrong command")
        return logged
