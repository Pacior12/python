import sqlite3
values = (
("Benjamin Sisko", "Human", 40),
("Jadzia Dax", "Trill", 300),
("Kira Nerys", "Bajoran", 29),
)
with sqlite3.connect("test_database.db") as connection:
    cursor = connection.cursor()
    cursor.execute("DROP TABLE IF EXISTS Roster")
    cursor.execute("""
    CREATE TABLE Roster(
        Name TEXT,
        Species TEXT,
        Age INT
    );"""
    )
    cursor.executemany("INSERT INTO Roster VALUES(?, ?, ?);", values)
    
    cursor.execute(
    "UPDATE Roster SET Name=? WHERE Species =?;",
    ("Ezri Dax", "Trill")
    )
    cursor.execute(
        "SELECT Age, Name FROM Roster WHERE Species = 'Bajoran';"
        )
    for row in cursor.fetchall():
        print(row)
