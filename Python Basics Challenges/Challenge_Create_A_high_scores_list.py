from pathlib import Path
import csv

file_path = Path.home() / "scores.csv"
file_path2 = Path.home() / "highest_scores.csv"
file_path2.touch()


with file_path.open(mode = "r", encoding = "utf-8", newline = "") as file:
    reader = csv.DictReader(file)
    scores = [row for row in reader]

high_scores = {}
for item in scores:
    name = item["name"]
    score = int(item["score"])
    if name not in high_scores:
        high_scores[name] = score
    else:
        if score > high_scores[name]:
            high_scores[name] = score
    

with file_path2.open(mode = "w", encoding = "utf-8") as file:
    writer = csv.DictWriter(file, fieldnames = ["name", "high_score"])
    writer.writeheader()
    for name in high_scores:
            row_dict = {"name": name, "high_score": high_scores[name]}
            writer.writerow(row_dict)
    

