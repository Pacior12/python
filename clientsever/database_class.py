import socket as s
import json
import time
import psycopg2
from notation_class import Notation


class Database:
    def __init__(self, db_host, db_name, db_user, db_password, db_port):
        self.db_host = db_host
        self.db_name = db_name
        self.db_user = db_user
        self.db_password = db_password
        self.db_port = db_port
        self.cur = None
        self.conn = None

    def connect_to_database(self):
        self.conn = psycopg2.connect(host=self.db_host, dbname=self.db_name, user=self.db_user,
                                     password=self.db_password, port=self.db_port)
        self.cur = self.conn.cursor()

    def create_table(self):
        self.cur.execute(""" CREATE TABLE IF NOT EXISTS users (
                name VARCHAR(255),
                password VARCHAR(255),
                if_admin BOOL,
                msg0 VARCHAR(255),
                msg0_sender VARCHAR(255),
                msg1 VARCHAR(255),
                msg1_sender VARCHAR(255),
                msg2 VARCHAR(255),
                msg2_sender VARCHAR(255),
                msg3 VARCHAR(255),
                msg3_sender VARCHAR(255),
                msg4 VARCHAR(255),
                msg4_sender VARCHAR(255),
                number_of_users_messages INTEGER
            )""")

    def add_an_account(self, name, password, if_admin):
        self.cur.execute("""
                INSERT INTO USERS(name, password, if_admin, number_of_users_messages)
                VALUES (%s, %s, %s, %s)
                """, (name, password, if_admin, "0"))
        self.conn.commit()

    def select_data_by_name(self, data, name):
        self.cur.execute(f"SELECT {data} FROM users WHERE name = %s", (name,))
        result = self.cur.fetchone()
        if result is not None:
            result = result[0]
        return result

    def select_all_data(self, data):
        self.cur.execute(f"SELECT {data} FROM users")
        result = self.cur.fetchall()
        return result

    def select_all_data_by_name(self, data, name):
        self.cur.execute(f"SELECT {data} FROM users WHERE name = %s", (name,))
        result = self.cur.fetchall()
        return result

    def update_data_by_name(self, data, data_input, name):
        self.cur.execute(f"UPDATE users SET {data} = %s WHERE name = %s", (data_input, name,))
        self.conn.commit()


    def delete_user_by_name(self, name):
        self.cur.execute(f"DELETE FROM users WHERE name = %s", (name,))
        self.conn.commit()


    def add_initial_admin_account(self):
        admin_user = self.select_data_by_name("name", "admin")
        if admin_user is None:
            self.add_an_account("admin", "1234", "yes")
