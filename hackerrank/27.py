superset = set(map(int, input().split()))

number_of_other_sets = int(input())

if_superset_of_all = True

for sets in range(number_of_other_sets):
    subset = set(map(int, input().split()))
    whether_superset = superset.issuperset(subset)
    if whether_superset == False:
        if_superset_of_all = False
    else:
        pass
    
    
print(if_superset_of_all)
