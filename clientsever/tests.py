import unittest
from database_class import Database
from notation_class import Notation
from server_class import Server

class Test_clientserver(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(Test_clientserver, self).__init__(*args, **kwargs)
        self.db_host = "localhost"
        self.db_name = "postgres"
        self.db_user = "postgres"
        self.db_password = "1234"
        self.db_port = 5432
        self.host = "127.0.0.1"
        self.port = 65432
        self.buffer = 1024
        self.max_number_of_messages = 5
        self.max_database_string_length = 255
        self.logged = False
        self.cur = None
        self.conn = None
        self.database = Database(self.db_host, self.db_name, self.db_user, self.db_password, self.db_port)
        self.notation = Notation()
        self.server = Server(self.host, self.port, self.buffer, self.database, self.notation, self.max_number_of_messages, self.max_database_string_length)

        
    def setUp(self):
        self.database.connect_to_database()
        self.database.create_table()
        self.database.add_initial_admin_account()

    def test_AddInitialAdmin(self):
        admin_name = self.database.select_data_by_name("name", "admin")
        self.assertEqual(admin_name, "admin")

    def test_DatabaseSelectDataByName(self):
        password_database = self.database.select_data_by_name("password", "admin")
        self.assertEqual(password_database, "1234")

    def test_Notation(self):
        test_string = "test_string"
        test_string_json = self.notation.encode_json(test_string)
        test_string_2 = self.notation.decode_json(test_string_json)
        self.assertEqual(test_string, test_string_2)
        
    def test_ServerLogin(self):
        login, logged, response = self.server.process_login_and_return_response(self.logged, "login", "admin", "1234")
        self.assertEqual(response, "Successfully logged in as admin")

    def test_RegisterAlreadyExistingUser(self):
        login, logged, response = self.server.process_login_and_return_response(self.logged, "register", "admin", "123456")
        self.assertEqual(response, "User already exists!")

    def test_PasswordTooLong(self):
        login, logged, response = self.server.process_login_and_return_response(self.logged, "register", "peja", "yeBhYOakZXiJB0dPcL2tLpDOdnrK9dN1mwdiXBgLm3tVZGoj1qASP3CSbzT7AIe0tdurxz1O5208nUaqEiy1VOlR6MLLoaphjBTlkSiDtlgt38vUuryJ4YdPQKZXXjNf8cY9XBfhXUssasWHk081xZUDXjVPcGqkZYTYPZkbNhDOb559ZNq1uFzinlNUWO56tjt0chgB2uAjnpuNO3UDAZvzO9HzZybnYCrVpCh6s7RaFQfVgf0IgUyhboMLwc85")
        self.assertEqual(response, "Password too long!")

    def test_CorrectRegister(self):
        login, logged, response = self.server.process_login_and_return_response(self.logged, "register", "tede", "1234")
        self.assertEqual(response, "Successfully logged in as tede")
        self.database.delete_user_by_name("tede")

    def test_makeAdmin(self):
        self.database.add_an_account("tede", "1234", False)
        response = self.server.make_admin(["make_admin", "tede"])
        self.assertEqual(response, "tede is now an admin")
        self.database.delete_user_by_name("tede")

    def test_deleteAdmin(self):
        self.database.add_an_account("tede", "1234", False)
        response = self.server.delete_admin(["delete_admin", "tede"])
        self.assertEqual(response, "tede is no longer an admin")
        self.database.delete_user_by_name("tede")

    def test_deleteUser(self):
        self.database.add_an_account("tede", "1234", False)
        response = self.server.delete_user(["delete_user", "tede"])
        self.assertEqual(response, "tede has been deleted")

    def test_sendingMessages(self):
        self.database.add_an_account("tede", "1234", False)
        logged, response = self.server.process_command_and_return_response(True, "admin", "message tede test")
        self.assertEqual(response, "Message delivered successfully!")
        message_sender = self.database.select_data_by_name("msg0_sender", "tede")
        self.assertEqual(message_sender, "admin")
        message = self.database.select_data_by_name("msg0", "tede")
        self.assertEqual(message, "test")
        number_of_messages = self.database.select_data_by_name("number_of_users_messages", "tede")
        self.assertEqual(number_of_messages, 1)
        self.database.delete_user_by_name("tede")
        
    def test_MessageboxFull(self):
        self.database.add_an_account("tede", "1234", False)
        logged, response1 = self.server.process_command_and_return_response(True, "admin", "message tede test1")
        logged, response2 = self.server.process_command_and_return_response(True, "admin", "message tede test2")
        logged, response3 = self.server.process_command_and_return_response(True, "admin", "message tede test3")
        logged, response4 = self.server.process_command_and_return_response(True, "admin", "message tede test4")
        logged, response5 = self.server.process_command_and_return_response(True, "admin", "message tede test5")
        logged, response6 = self.server.process_command_and_return_response(True, "admin", "message tede test6")
        self.assertEqual(response1, "Message delivered successfully!")
        self.assertEqual(response2, "Message delivered successfully!")
        self.assertEqual(response3, "Message delivered successfully!")
        self.assertEqual(response4, "Message delivered successfully!")
        self.assertEqual(response5, "Message delivered successfully!")
        self.assertEqual(response6, "Recipient's message box is full!")
        self.database.delete_user_by_name("tede")

    def test_readMessages(self):
        self.database.add_an_account("tede", "1234", False)
        logged, response1 = self.server.process_command_and_return_response(True, "admin", "message tede test1")
        logged, response2 = self.server.process_command_and_return_response(True, "tede", "read_messages")
        self.assertEqual(response2, {"1. admin": "test1"})
        self.database.delete_user_by_name("tede")
            
    def test_logout(self):
        logged, response = self.server.process_command_and_return_response(True, "admin", "logout")
        self.assertEqual(logged, False)
        self.assertEqual(response, "You logged out successfully")
        
    def test_version(self):
        logged, response = self.server.process_command_and_return_response(True, "admin", "version")
        self.assertEqual(response, "This is a 1.1 version of this application")

    def tearDown(self):
        self.database.delete_user_by_name("tede")
                               

if __name__ == "__main__":
    unittest.main()
