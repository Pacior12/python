#Let the user choose the level

import random

print("Welcome to hangman!")
print("Choose a mode")
print("For singleplayer mode(7 lives) -  press 1")
print("For multiplayer mode(7 lives) -  press 2")
print("For hardcore singleplayer mode (3 lives) - press 3")

#defining a function to let a player choose, which gamemode they want to play

def choice():
    while (1 > 0):
        game_mode = input("Enter a digit to choose a level: ")
        if game_mode == "1":
            return "1"
            break
        if game_mode == "2":
            return "2"
            break
        if game_mode == "3":
            return "3"
            break
#checking if user's password contains digits
def contains_number(selected_word):
    return any(char.isdigit() for char in selected_word)

#printing certain stages of "Hangman" for a value of mistakes
def mistake(mistakes):
    if mistakes == 1:
        print("\n")
        print("\n")
        print("\n")
        print("\n")
        print("========")
    if mistakes == 2:
        print("\n")
        print("\n")
        print("   ||   ")
        print("   ||   ")
        print("   ||   ")
        print("   ||   ")
        print("========")
    if mistakes == 3:
        print("\n")
        print("   ||   ")
        print("   ||   ")
        print("   ||   ")
        print("   ||   ")
        print("   ||   ")
        print("   ||   ")
        print("   ||   ")
        print("   ||   ")
        print("========")
    if mistakes == 4:
        print("\n")
        print("   ||=========|   ")
        print("   ||   ")
        print("   ||   ")
        print("   ||   ")
        print("   ||   ")
        print("   ||   ")
        print("   ||   ")
        print("   ||   ")
        print("========")
    if mistakes == 5:
        print("\n")
        print("   ||=========|   ")
        print("   ||         |")
        print("   ||         |")
        print("   ||   ")
        print("   ||   ")
        print("   ||   ")
        print("   ||   ")
        print("   ||   ")
        print("========")
    

    if mistakes == 6:
        print("\n")
        print("   ||=========|")
        print("   ||         |")
        print("   ||         |")
        print("   ||         O")
        print("   ||        \|/")
        print("   ||   ")
        print("   ||   ")
        print("   ||   ")
        print("========")

    if mistakes == 7:
        print("\n")
        print("   ||=========|")
        print("   ||         |")
        print("   ||         |")
        print("   ||         O")
        print("   ||        \|/")
        print("   ||         | ")
        print("   ||        / \ ")
        print("   ||   ")
        print("========")
    
list_of_words = ["secretary", "servant" , "tub", "wire", "crown", "wood", "plantation", "pipe", "base", "grass", "propety", "impulse", "lumber", "women", "story", "frogs", "invention", "death", "toe", "hydrant", "drawer", "toes", "distance", "time", "horn", "activity", "cast", "grape", "blood", "top", "jelly", "needle", "quartz", "reason", "slave", "support", "muscle", "sidewalk", "error", "crayon", "window", "pancake", "tomatoes", "thumb", "cats", "head", "field", "existence", "history", "glove"]

n = choice()

#variables needed later
mistakes = 0
letter = 0

#picking a random letter from a list of words
def pick_a_random_word(list_of_words):
    random_word_index = random.randint(0, 49)
    random_word = list_of_words[random_word_index]
    return random_word

#converting a word into '_'
def word_converter(random_word):
    x = (len(random_word) * "_")
    return x

def correct_guess():
    print ("Correct letter!")

def incorrect_guess():
    print("Incorrect letter!")

#Adding letters in a place of '_' if guess is correct
def letter_adder(random_word2, chosen_letter):
    random_word2 = random_word2[:i] + chosen_letter.lower() + random_word2[i:]
    random_word2 = random_word2[:i+1] + random_word2[i+2:]
    return random_word2

def win():
    print("You win!")

def lose(random_word):
    print("You died! :(")
    print(f"The correct word was {random_word}")

    

#player choosing option 1
if n == "1":
    random_word = pick_a_random_word(list_of_words)
    random_word2 = word_converter(random_word)
    while (mistakes != 7) or (random_word2 != random_word):
        print(random_word2)
        if letter == 1:
            correct_guess()
        elif letter == 2:
            incorrect_guess()
        chosen_letter = input("Choose a letter: ")
        for i in range(0, len(random_word)):
            if chosen_letter.lower() == random_word[i]:
                random_word2 = letter_adder(random_word2, chosen_letter)
                letter = 1
                continue
            elif (random_word.find(chosen_letter.lower()) == -1):
                mistakes = mistakes + 1
                mistake(mistakes)
                letter = 2
                break
        if mistakes == 7:
            lose(random_word)
            break
        if random_word == random_word2:
            win()
            break
        
#player choosing option 2                    
if n == "2":
    random_word = input("Write a password for player 2: ")
    random_word = random_word.lower()
    if contains_number(random_word) == True:
        print("Password cannot contain digits!")
    else:
        random_word2 = word_converter(random_word)
        for i in range(0, len(random_word)):
            if random_word[i] == " ":
                random_word2 = random_word2[:i] + " " + random_word2[i:]
                random_word2 = random_word2[:i+1] + random_word2 [i+2:] 
        while (mistakes != 7) or (random_word2 != random_word):
            print(random_word2)
            if letter == 1:
                correct_guess()
            elif letter == 2:
                incorrect_guess()
            chosen_letter = input("Choose a letter: ")
            for i in range(0, len(random_word)):
                if chosen_letter.lower() == random_word[i]:
                    random_word2 = letter_adder(random_word2, chosen_letter)
                    letter = 1
                    continue
                elif (random_word.find(chosen_letter.lower()) == -1):
                    mistakes = mistakes + 1
                    mistake(mistakes)
                    letter = 2
                    break
            if mistakes == 7:
                lose(random_word)
                break
            if random_word == random_word2:
                win()
                break
        
#player choosing option 3
            
if n == "3":
    random_word = pick_a_random_word(list_of_words)
    random_word2 = word_converter(random_word)
    while (mistakes != 7) or (random_word2 != random_word):
        if letter == 1:
            correct_guess()
        elif letter == 2:
            incorrect_guess()
        print(random_word2)
        chosen_letter = input("Choose a letter: ")
        for i in range(0, len(random_word)):
            if chosen_letter.lower() == random_word[i]:
                random_word2 = letter_adder(random_word2, chosen_letter)
                letter = 1
                continue
            elif (random_word.find(chosen_letter.lower()) == -1):
                mistakes = ((2 * mistakes) + 1)
                mistake(mistakes)
                letter = 2
                break
        if mistakes == 7:
            lose(random_word)
            break
        if random_word == random_word2:
            win()
            break
        
    

