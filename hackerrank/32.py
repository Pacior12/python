import numpy

N , M = map(int , input().split())

arr = []

j = 0

for i in range(N):

    if j < M:
        value = list(map(int , input().split(" ")))
        arr.append(value)
        j+=1
    else:
        print("error!")


my_array = numpy.array(arr)
column_sum = numpy.sum(my_array , axis= 0)

print(numpy.product(column_sum))
