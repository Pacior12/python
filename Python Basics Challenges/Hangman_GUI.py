import random
import tkinter as tk
from tkinter import *
import easygui as gui
from functools import partial
import time
 
 
list_of_words = ["secretary", "servant" , "tub", "wire", "crown", "wood", "plantation", "pipe", "base", "grass", "propety", "impulse", "lumber", "women", "story", "frogs", "invention", "death", "toe", "hydrant", "drawer", "toes", "distance", "time", "horn", "activity", "cast", "grape", "blood", "top", "jelly", "needle", "quartz", "reason", "slave", "support", "muscle", "sidewalk", "error", "crayon", "window", "pancake", "tomatoes", "thumb", "cats", "head", "field", "existence", "history", "glove"]
hangmans = [
    "        ",
    "        ",
    "        ",
    "        ",
    "        ",
    "        ",
    "        ",
    "        ",
    "        ",
    "        ",
    "        ",
    "        ",
    "        ",
    "        ",
    "        ",
    "        ",
    "        ",
    "========",
    "        ",
    "        ",
    "        ",
    "        ",
    "   ||   ",
    "   ||   ",
    "   ||   ",
    "   ||   ",
    "========",
    "   ||   ",
    "   ||   ",
    "   ||   ",
    "   ||   ",
    "   ||   ",
    "   ||   ",
    "   ||   ",
    "   ||   ",
    "========",
    "   ||=========|   ",
    "   ||   ",
    "   ||   ",
    "   ||   ",
    "   ||   ",
    "   ||   ",
    "   ||   ",
    "   ||   ",
    "========",
    "   ||=========|",
    "   ||         |",
    "   ||         |",
    "   ||   ",
    "   ||   ",
    "   ||   ",
    "   ||   ",
    "   ||   ",
    "========",
    "   ||=========|",
    "   ||         |",
    "   ||         |",
    "   ||         O",
    "   ||        \|/",
    "   ||   ",
    "   ||   ",
    "   ||   ",
    "========",
    "   ||=========|",
    "   ||         |",
    "   ||         |",
    "   ||         O",
    "   ||        \|/",
    "   ||         | ",
    "   ||        / \ ",
    "   ||   ",
    "========"
]
def error_message():
    gui.msgbox(msg = "The input needs to be only one letter at a time and you can't use the same letter twice")
    
 
 
def select_a_random_word(list_of_words):
    random_word_index = random.randint(0, 49)
    random_word = list_of_words[random_word_index]
    return random_word
 
def create_game_window(random_word, random_word2, list_of_used_letters, mistakes_counter, mode):
    game_window = tk.Tk()
    game_window.title("Hangman")
 
    game_window.rowconfigure([0, 2, 3, 4, 5, 6], minsize = 40, weight = 1)
    game_window.rowconfigure(1, minsize = 120, weight = 1)
    game_window.columnconfigure([0, 1, 2], minsize = 40, weight = 1)
 
    mode_message = tk.Label()
    mode_message.grid(row=0, column = 1, sticky = "nesw")
 
    hangman_picture = tk.Label()
    hangman_picture.grid(row = 1, column = 1, sticky = "nesw")
 
    password_message = tk.Label(text = "Password: ")
    password_message.grid(row = 2, column = 0)
 
    mistakes_counter_label = tk.Label(text=f"Mistakes: {mistakes_counter[0]}")
    mistakes_counter_label.grid(row=6, column=1)
 
    show_password = tk.Label()
    show_password.grid(row = 2, column = 1)
 
    already_used_letters_message = tk.Label(text = "Already used letters: ")
    already_used_letters_message.grid(row = 3, column = 0)
 
    show_already_used_letters = tk.Label()
    show_already_used_letters.grid(row = 3, column = 1)
 
    enter_a_letter_message = tk.Label(text = "Enter a letter: ")
    enter_a_letter_message.grid(row = 4, column = 0)
 
    enter_a_letter_entry = tk.Entry(width = 80)
    enter_a_letter_entry.grid(row = 4, column = 1)
 
    check_letter_button = tk.Button(text="Check letter", command=lambda: check_letter(enter_a_letter_entry, random_word, random_word2, show_password, show_already_used_letters, hangman_picture, list_of_used_letters, mistakes_counter, mistakes_counter_label, game_window, mode))
    check_letter_button.grid(row=5, column=1)
 
    possible_mistakes_label = tk.Label()
    possible_mistakes_label.grid(row = 6, column = 0)
    
    return show_password, show_already_used_letters, hangman_picture, mode_message, enter_a_letter_entry, mistakes_counter_label, possible_mistakes_label
    
 
def check_letter(enter_a_letter_entry, random_word_list, random_word_list_2, show_password, show_already_used_letters, hangman_picture, list_of_used_letters, mistakes_counter, mistakes_counter_label, game_window, mode):
    letter = enter_a_letter_entry.get()
    print(mistakes_counter)
    if len(letter) != 1 or not letter.isalpha() or letter in list_of_used_letters:
        error_message()
        enter_a_letter_entry.delete(0, tk.END)
    else:
        enter_a_letter_entry.delete(0, tk.END)
        append_variable = 0
        if letter.lower() in random_word_list:
            for i in range(len(random_word_list)):
                if letter.lower() == random_word_list[i]:
                    random_word_list_2[i] = letter.lower()
            enter_a_letter_entry.delete(0, tk.END)
            list_of_used_letters.append(letter)
            list_for_print = ''.join(random_word_list_2)
            show_password["text"] = f"{list_for_print}"
            show_already_used_letters["text"] = f"{list_of_used_letters}"
        else:
            mistakes_counter[0] += 1
            mistakes_counter_label["text"] = f"Mistakes: {mistakes_counter[0]}"
            if mode == "normal":
                hangman_picture.config(text='\n'.join(hangmans[mistakes_counter[0] * 9:(mistakes_counter[0] + 1) * 9]))
            if mode == "hardcore":
                hangman_picture.config(text='\n'.join(hangmans[(mistakes_counter[0] + 4) * 9:(mistakes_counter[0] + 5) * 9]))

            list_of_used_letters.append(letter)
            show_already_used_letters["text"] = f"{list_of_used_letters}"
    if mistakes_counter[0] == 7 and mode == "normal":
        result = "lose"
        word = ''.join(random_word_list)
        game_window.destroy()
        end_game_message(result, word)
    if mistakes_counter[0] == 3 and mode == "hardcore":
        result = "lose"
        word = ''.join(random_word_list)
        game_window.destroy()
        end_game_message(result, word)
    if random_word_list == random_word_list_2:
        result = "win"
        word = ''.join(random_word_list)
        game_window.destroy()
        end_game_message(result, word)                                 
    
def normal_mode(start_window):
    mode = "normal"
    start_window.destroy()
    random_word = select_a_random_word(list_of_words)
    print(random_word)
    random_word_list = []
    random_word_list_2 = []
    random_word2 = (len(random_word) * "_")
    for i in range(len(random_word)):
        random_word_list.append(random_word[i])
    for i in range(len(random_word)):
        random_word_list_2.append("_")    
    print(random_word_list)
    print(random_word_list_2)
    list_of_used_letters = []
    mistakes_counter = [0]
    show_password, show_already_used_letters, hangman_picture, mode_message, enter_a_letter_entry, mistakes_counter_label, possible_mistakes_label = create_game_window(random_word_list, random_word_list_2, list_of_used_letters, mistakes_counter, mode)
    mode_message["text"] = "Normal mode"
    show_password["text"] = f"{random_word2}"
    show_already_used_letters["text"] = f"{list_of_used_letters}"
    possible_mistakes_label["text"] = f"Possible mistakes: 6"
 
 
    
    
    
def end_game_message(result, random_word):
    if result == "win":
        result_word = "won"
    if result == "lose":
        result_word = "lost"
    end_game_window = tk.Tk()
    end_game_window.rowconfigure([0, 1, 2], minsize = 40, weight = 1)
    end_game_window.columnconfigure([0, 1, 2], minsize = 40, weight = 1)
 
    game_ending_message = tk.Label(text = f"You {result_word}!")
    game_ending_message.grid(row=0, column=1, sticky="nesw")
    
 
    game_ending_correct_password = tk.Label(text = f"The password was '{random_word}'")
    game_ending_correct_password.grid(row=1, column=1, sticky="nesw")
 
    back_to_menu_button = tk.Button(text="Back to main menu", command=lambda: create_start_window(end_game_window))
    back_to_menu_button.grid(row = 2, column = 0)
 
    quit_button = tk.Button(text = "  Quit game   ", command = lambda: quit())
    quit_button.grid(row = 2, column = 2)
    
 
 
 
def hardcore_mode(start_window):
    mode = "hardcore"
    start_window.destroy()
    random_word = select_a_random_word(list_of_words)
    print(random_word)
    random_word_list = []
    random_word_list_2 = []
    random_word2 = (len(random_word) * "_")
    for i in range(len(random_word)):
        random_word_list.append(random_word[i])
    for i in range(len(random_word)):
        random_word_list_2.append("_")    
    print(random_word_list)
    print(random_word_list_2)
    list_of_used_letters = []
    mistakes_counter = [0]
    show_password, show_already_used_letters, hangman_picture, mode_message, enter_a_letter_entry, mistakes_counter_label, possible_mistakes_label = create_game_window(random_word_list, random_word_list_2, list_of_used_letters, mistakes_counter, mode)
    mode_message["text"] = "Hardcore mode"
    show_password["text"] = f"{random_word2}"
    show_already_used_letters["text"] = f"{list_of_used_letters}"
    possible_mistakes_label["text"] = f"Possible mistakes: 2"
 

 
def create_start_window(root=None):
    if root is None:
        root = tk.Tk()
        root.title("Hangman")
 
    root.rowconfigure([0, 1, 2], minsize=40, weight=1)
    root.columnconfigure([0, 1, 2], minsize=100, weight=1)
 
    welcome_to_python_message = tk.Label(root, text="Welcome to hangman!")
    welcome_to_python_message.grid(row=0, column=1)
 
    choose_game_mode_message = tk.Label(root, text=" Choose a gamemode! ")
    choose_game_mode_message.grid(row=1, column=1)
 
    normal_game_button = tk.Button(root, text="Normal gamemode", command=lambda: normal_mode(root))
    normal_game_button.grid(row=2, column=0)
 
    hardcore_game_button = tk.Button(root, text="Hardcore gamemode", command=lambda: hardcore_mode(root))
    hardcore_game_button.grid(row=2, column=2)
 
    return root
 
 
create_start_window().mainloop()
